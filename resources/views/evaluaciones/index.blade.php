@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    Evaluaciones
@endsection

@section('content')
<div class="row">
    <evaluacion></evaluacion>
</div>
    
@endsection