@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    <i class="fas fa-database"></i>
    Base de datos
@endsection

@section('content')
<div class="row">
    @foreach ($medicions as $medicion)
        <div class="col-12 col-xl-4 col-lg-6 col-md-6  mt-2">
            <div class="card mb-4">
                <div class="card-header text-center" style="color:#024c90">
                    <h5 class="card-title">{{ $medicion->descripcion_corto }}</h5>
                </div>
                <div class="card-body">
                    <div class="text-center" style="font-size: 88px; color: Dodgerblue;">
                        <i class="fa fa-database"></i>
                    </div>
                </div>
                <div class="card-header text-center">
                    <a href="{{ route('base.export', [$medicion->id, 1]) }}" class="btn btn-lider"><i class="fas fa-download m-1"></i> Lider</a>
                    <a href="{{ route('base.export', [$medicion->id, 2]) }}" class="btn btn-express"><i class="fas fa-download m-1"></i> Express</a>
                    <a href="{{ route('base.export', [$medicion->id, 3]) }}" class="btn btn-sba"><i class="fas fa-download m-1"></i> SBA</a>
                </div>
            </div>
        </div>

    @endforeach

    {{-- <descarga-db></descarga-db> --}}

</div>
    
@endsection