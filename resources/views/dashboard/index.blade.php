@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css">

@endsection

@section('titulo')
    Dashboard
@endsection

@section('content')
<div class="row">
    <dashboard></dashboard>
</div>
    
@endsection