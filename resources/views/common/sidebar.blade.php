<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                {{-- <div class="sb-sidenav-menu-heading">Core</div> --}}
                <a class="{{ request()->is('dashboard') ? "nav-link active" : "nav-link" }}"  href="{{ route('dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <a class="{{ request()->is('punto_contacto') ? "nav-link active" : "nav-link" }}" href="{{ route('punto.contacto') }}">
                    <div class="sb-nav-link-icon">
                        {{-- <i class="fas fa-tachometer-alt"></i> --}}
                        <i class="fas fa-cart-arrow-down"></i>
                    </div>
                    Puntos de contacto
                </a>
                <a class="{{ request()->is('variedadad_por_local') ? "nav-link active" : "nav-link" }}" href="{{ route('variedad.por.local') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-utensils"></i>
                    </div>
                    Disponibilidad productos quincenal
                </a>
                <a class="{{ request()->is('variedadad_por_seccion') ? "nav-link active" : "nav-link" }}" href="{{ route('variedad.por.seccion') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-utensils"></i>
                    </div>
                    Disponibilidad productos acumulado
                </a>
                
                <a class="{{ request()->is('ranking_local') ? "nav-link active" : "nav-link" }}"  href="{{ route('ranking_local') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-signal"></i>
                    </div>
                    Ranking por local
                </a>

                <a class="{{ request()->is('evaluaciones') ? "nav-link active" : "nav-link" }}"  href="{{ route('evaluacion.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-file-signature"></i>
                    </div>
                    Evaluaciones
                </a>

                <a class="{{ request()->is('benchmark') ? "nav-link active" : "nav-link" }}" href="{{ route('benchmark') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-chart-line"></i>
                    </div>
                    Benchmark(base 0)
                </a>
                <a class="{{ request()->is('ficha') ? "nav-link active" : "nav-link" }}" href="{{ route('ficha.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-paste"></i>
                    </div>
                    Ficha Metodológica
                </a>
                <a class="{{ request()->is('base') ? "nav-link active" : "nav-link" }}" href="{{ route('base.index') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-database"></i>
                    </div>
                    Base de Datos
                </a>
                
                @can('isAdmin')
                    <div class="sb-sidenav-menu-heading">Administración</div>
                    <a class="{{ request()->is('users') ? "nav-link active" : "nav-link" }}" href="{{ route('user.index') }}">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-friends"></i>
                        </div>
                        Usuarios
                    </a>
                    <a class="{{ request()->is('dooit*') ? "nav-link" : "nav-link collapsed" }}" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="true" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        Dooit
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="{{ request()->is('dooit*') ? "collapse show" : "collapse" }}" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="{{ request()->is('dooit/actualiza') ? "nav-link active" : "nav-link" }}" href="{{ route('dooit') }}">Actualización</a>
                        </nav>
                    </div>
                    <div class="{{ request()->is('dooit*') ? "collapse show" : "collapse" }}" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="{{ request()->is('dooit/datos_detalle') ? "nav-link active" : "nav-link" }}" href="{{ route('datos.detalle') }}">Datos Detalle</a>
                        </nav>
                    </div>
                    {{-- <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                        <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                        Mantenedores
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a> --}}
                    {{-- <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                Authentication
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="login.html">Login</a>
                                    <a class="nav-link" href="register.html">Register</a>
                                    <a class="nav-link" href="password.html">Forgot Password</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                Error
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="401.html">401 Page</a>
                                    <a class="nav-link" href="404.html">404 Page</a>
                                    <a class="nav-link" href="500.html">500 Page</a>
                                </nav>
                            </div>
                        </nav>
                    </div> --}}
                    {{-- <div class="sb-sidenav-menu-heading">Addons</div>
                    <a class="nav-link" href="charts.html">
                        <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                        Charts
                    </a>
                    <a class="nav-link" href="tables.html">
                        <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                        Tables
                    </a> --}}
                @endcan
            </div>
        </div>
        
    </nav>
</div>