@php
    function cambiaValorSiNo($valor) {
        $resp = null;
        if ($valor == 1){
            $resp = 1;
        }
        else if ($valor == 2){
            $resp = 0;
        }
        else if ($valor == 99){
            $resp = "-";
        }
        return $resp;

    }

    $meses = [
        1 => 'Enero',
        2 => 'Febrero',
        3 => 'Marzo',
        4 => 'Abril',
        5 => 'Mayo',
        6 => 'Junio',
        7 => 'Julio',
        8 => 'Agosto',
        9 => 'Septiembre',
        10 => 'Octubre',
        11 => 'Noviembre',
        12 => 'Diciembre',
    ];

@endphp

<table>
    <thead>
        <tr>
            <th style="background: #4472c4">cod_tarea</th>
            <th style="background: #4472c4">[FEC_EVAL] Fecha</th>
            <th style="background: #4472c4">[HR_ENTRADA] Hora entrada al local</th>
            <th style="background: #4472c4">[HR_SALIDA] Hora salida del local</th>
            <th style="background: #4472c4">Código local</th>
            <th style="background: #4472c4">[NOM_LOCAL] Local</th>
            <th style="background: #4472c4">QUINCENA</th>
            <th style="background: #4472c4">MES</th>
            <th style="background: #4472c4">AÑO</th>

            <th style="background: #4472c4">P1_1_1 ¿Se observa el sector de frutas y verduras limpio?(vitrinas/góndolas/estanterías/piso)</th>
            <th style="background: #4472c4">P1_1_1_TXT </th>
            <th style="background: #4472c4">P1_1_2 ¿Existe disponibilidad y es fácil encontrar las bolsas para el pesado de los productos?</th>
            <th style="background: #4472c4">P1_1_2_TXT</th>
            <th style="background: #4472c4">P1_1_3 ¿Hay fila para pesar en balanzas?</th>
            <th style="background: #4472c4">P1_1_4 ¿Se observan las balanzas limpias?</th>
            <th style="background: #4472c4">P1_1_4_TXT</th>
            <th style="background: #4472c4">P1_1_5 ¿Se observan las balanzas sin etiquetas de precio pegadas?</th>
            <th style="background: #4472c4">P1_1_5_TXT</th>
            <th style="background: #4472c4">P1_1_6 ¿Los precios están ordenandos  y corresponden al producto mostrado?</th>
            <th style="background: #4472c4">P1_1_6_TXT</th>
            <th style="background: #4472c4">P1_2_1 ¿Las frutas/verduras se aprecian ordenadas? (están en el lugar que corresponde y no se mezclan entre sí)</th>
            <th style="background: #4472c4">P1_2_1_TXT</th>
            <th style="background: #4472c4">P1_2_2 ¿Las frutas/verduras se aprecian en buen estado? (no podridas/sin golpes/sin tierra/partidas)</th>
            <th style="background: #4472c4">P1_2_2_TXT</th>
            <th style="background: #4472c4">P1_2_3 ¿Hay suficiente stock de frutas y verduras? (No se vean lugares vacíos o con falta de producto)</th>
            <th style="background: #4472c4">P1_2_3_TXT</th>

            <th style="background: #4472c4">P1_2_4 ¿Se encuentran las siguientes frutas en el sector?</th>            
            <th style="background: #60eb7e">P1_2_4 Plátano</th>
            <th style="background: #60eb7e">P1_2_4 Palta hass</th>
            <th style="background: #60eb7e">P1_2_4 Palta (otras variedades no hass: fuerte, edranol, negra)</th>
            <th style="background: #60eb7e">P1_2_4 Limón</th>
            <th style="background: #60eb7e">P1_2_4 Limón (otras variedades: pica, sutil, gourmet)</th>
            <th style="background: #60eb7e">P1_2_4 Naranja</th>
            <th style="background: #60eb7e">P1_2_4 Naranja malla </th>
            <th style="background: #60eb7e">P1_2_4 Manzana</th>
            <th style="background: #60eb7e">P1_2_4 Pomelo</th>
            <th style="background: #60eb7e">P1_2_4 Mango</th>
            <th style="background: #60eb7e">P1_2_4 Piña</th>
            <th style="background: #60eb7e">P1_2_4 Pera</th>
            <th style="background: #60eb7e">P1_2_4 Kiwi</th>
            <th style="background: #60eb7e">P1_2_4 Frutilla</th>
            <th style="background: #60eb7e">P1_2_4 Arándanos</th>
            {{-- <th style="background: #60eb7e">P1_2_4 Plátano barraganete</th> --}}
            {{-- <th style="background: #60eb7e">P1_2_4 Palta</th> --}}

            <th style="background: #4472c4">P1_2_5 ¿Se encuentran las siguientes verduras en el sector?</th>
            <th style='background: #60eb7e'>P1_2_5 Zanahoria</th>
            <th style='background: #60eb7e'>P1_2_5 Tomate cocktail (cherry)</th>
            <th style='background: #60eb7e'>P1_2_5 Repollo</th>
            <th style='background: #60eb7e'>P1_2_5 Brócoli</th>
            <th style='background: #60eb7e'>P1_2_5 Zapallo trozo</th>
            <th style='background: #60eb7e'>P1_2_5 Dientes de dragón</th>
            <th style='background: #60eb7e'>P1_2_5 Cebollín</th>
            <th style='background: #60eb7e'>P1_2_5 Champiñón</th>
            <th style='background: #60eb7e'>P1_2_5 Ensaladas preparadas</th>
            <th style='background: #60eb7e'>P1_2_5 Pimiento Verde</th>
            <th style='background: #60eb7e'>P1_2_5 Pimiento Rojo</th>
            <th style='background: #60eb7e'>P1_2_5 Lechuga escarola</th>
            <th style='background: #60eb7e'>P1_2_5 Apio</th>
            <th style='background: #60eb7e'>P1_2_5 Cebolla</th>
            <th style='background: #60eb7e'>P1_2_5 Ajo</th>
            <th style='background: #60eb7e'>P1_2_5 Papas</th>
            <th style='background: #60eb7e'>P1_2_5 Zapallo entero</th>
            <th style='background: #60eb7e'>P1_2_5 Tomate </th>
            {{-- <th style='background: #60eb7e'>P1_2_5 Lechuga</th> --}}

            <th style="background: #4472c4">P1_3_1 ¿El uniforme del personal se aprecia limpio y ordenado?</th>
            <th style="background: #4472c4">P1_3_1_TXT</th>
            <th style="background: #4472c4">P1_3_2 ¿El personal de atención cuenta con guantes y mascarilla?</th>
            <th style="background: #4472c4">P1_3_2_TXT</th>
            <th style="background: #4472c4">FOTO_FFVV</th>
            
            <th style="background: #4472c4">P0_2</th>
            <th style="background: #4472c4">P2_1_1_1 [Mesón] ¿Se observa el sector de carnicería limpio?(vitrinas/piso)</th>
            <th style="background: #4472c4">P2_1_1_1_T</th>
            <th style="background: #4472c4">P2_1_1_2 [Mesón] ¿Los precios están ordenandos  y corresponden al producto mostrado?</th>
            <th style="background: #4472c4">P2_1_1_2_T</th>
            <th style="background: #4472c4">P2_1_2_1 [Mesón] ¿Las carnes se aprecian ordenados? (están en el lugar que corresponde y no se mezclan entre sí)</th>
            <th style="background: #4472c4">P2_1_2_1_T</th>
            <th style="background: #4472c4">P2_1_2_2 [Mesón] ¿Las carnes se aprecian en buen estado? (buen color/sin liquidos)</th>
            <th style="background: #4472c4">P2_1_2_2_T</th>
            <th style="background: #4472c4">P2_1_2_3 [Mesón] ¿Hay suficiente stock de carnes? (No se vean lugares vacíos o con falta de producto)</th>
            <th style="background: #4472c4">P2_1_2_3_T</th>
            
            <th style="background: #4472c4">P2_1_2_6 [Mesón] ¿Se encuentran los siguientes productos en el mesón?</th>
            <th style='background: #60eb7e'>P2_1_2_6 Pollo Trutro entero</th>
            <th style='background: #60eb7e'>P2_1_2_6 Pollo pechuga pollo</th>
            <th style='background: #60eb7e'>P2_1_2_6 Pollo trutro ala</th>
            <th style='background: #60eb7e'>P2_1_2_6 Pavo Trutro corto sin hueso</th>
            <th style='background: #60eb7e'>P2_1_2_6 Cerdo Pulpa con hueso</th>
            <th style='background: #60eb7e'>P2_1_2_6 Cerdo chuleta vetada</th>
            <th style='background: #60eb7e'>P2_1_2_6 Cerdo chuleta centro</th>

            <th style="background: #4472c4">P2_1_2_4 [Mesón] ¿Existe oferta de carbón como producto adicional?</th>
            <th style="background: #4472c4">P2_1_2_4_T</th>
            <th style="background: #4472c4">P2_1_3_1 [Mesón] Presencia de colaboradores en vitrina / mesión de la sección</th>
            <th style="background: #4472c4">P2_1_3_1_T</th>
            <th style="background: #4472c4">P2_1_3_2 [Mesón] Cantidad de personal de atención</th>
            <th style="background: #4472c4">P2_1_3_3 [Mesón] ¿El uniforme del personal se aprecia limpio y ordenado?</th>
            <th style="background: #4472c4">P2_1_3_3_T</th>
            <th style="background: #4472c4">P2_1_3_4 [Mesón] ¿El personal de atención cuenta con guantes y mascarilla?</th>
            <th style="background: #4472c4">P2_1_3_4_T</th>

            <th style="background: #4472c4">P2_2_1_1 [Envasado] ¿Se observa el sector de carnicería limpio?(vitrinas/piso)</th>
            <th style="background: #4472c4">P2_2_1_1_T</th>
            <th style="background: #4472c4">P2_2_1_2 [Envasado] ¿Los precios están ordenandos  y corresponden al producto mostrado?</th>
            <th style="background: #4472c4">P2_2_1_2_T</th>
            <th style="background: #4472c4">P2_2_2_1 [Envasado] ¿Está ordenado por tipos de carne? (están en el lugar que corresponde y no se mezclan entre sí)</th>
            <th style="background: #4472c4">P2_2_2_1_T</th>
            <th style="background: #4472c4">P2_2_2_2 [Envasado] ¿Las carnes en aprecian en buen estado? (buen color/sin liquidos)</th>
            <th style="background: #4472c4">P2_2_2_2_T</th>
            <th style="background: #4472c4">P2_2_2_3 [Envasado] ¿Hay suficiente stock de carnes? (No se vean lugares vacíos o con falta de producto)</th>
            <th style="background: #4472c4">P2_2_2_3_T</th>

            <th style="background: #4472c4">P2_2_2_6 [Envasado] ¿Se encuentran los siguientes productos básicos en el sector?</th>
            <th style='background: #60eb7e'>P2_2_2_6 Mercadeo(Bustec o Churrasco)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Trozo buen corte(Postas)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Variedades(Hamburguesa)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Molidas(Tartaro 500g, Corriente 500g o Especial 500g)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Fino(Vetado o Filete)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Olla(Postas, Tapapecho o Choclillo)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Asados(Sobrecostilla, Huachalomo o Posta paleta)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Cerdo(Costillar, Pulpa o Plateada)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Pavo(Molida o pechuga)</th>
            <th style='background: #60eb7e'>P2_2_2_6 Pollo(Filetito, Pechuga o Trutros o Pollo entero)</th>

            <th style="background: #4472c4">P2_2_2_7 [Envasado] ¿Se encuentran los siguientes productos en el sector?</th>
            <th style='background: #60eb7e'>P2_2_2_7 Carnes premium(Vetado, Tapabarriga o Liso)</th>
            <th style='background: #60eb7e'>P2_2_2_7 Al palo(Asado tradicional, Estomaguillo, Plateada o Entraña)</th>
            <th style='background: #60eb7e'>P2_2_2_7 Americano(Abastero, Punta ganso, Punta picana o entraña)</th>
            <th style='background: #60eb7e'>P2_2_2_7 Meatless(Molida, Hamburguesa, salchicha)</th>
            <th style='background: #60eb7e'>P2_2_2_7 Cortes con hueso(Tomahawk, Entrecot/porterhouse o Chuletas con hueso)</th>
            <th style='background: #60eb7e'>P2_2_2_7 Cerdo(Malaya)</th>
            <th style='background: #60eb7e'>P2_2_2_7 Pollo sin marinar(Pechuga o Filetito)</th>

            <th style="background: #4472c4">P2_2_2_4 [Envasado] ¿Se aprecia que todos las carnes envasadas cuentan con su etiquetado?</th>
            <th style="background: #4472c4">P2_2_2_4_T</th>
            <th style="background: #4472c4">P2_2_2_5 [Envasado] ¿Hay al menos UNA MARCA PREMIUM? (Corrales del sur - Torobayo - Devesa -  BlackBeef - Corral Osorno - Cabaña Las Lilas o Angus by Gorina)</th>
            <th style="background: #4472c4">P2_2_2_5_T</th>
            <th style="background: #4472c4">P2_1_2_5 ¿Hay presencia de pollos asados?</th>
            <th style="background: #4472c4">P2_1_2_5_T</th>
            <th style="background: #4472c4">FOTO_CARNE</th>
            <th style="background: #4472c4">P3_1_1 [Mesón] ¿Se observa el sector de fiambrería limpio?(vitrinas (vidrios)/piso/máquinas utilizadas/bandejas donde de ponen los productos)</th>
            <th style="background: #4472c4">P3_1_1_TXT</th>
            <th style="background: #4472c4">P3_1_3 [Mesón] ¿El porta numero de atención a cliente está visible, de fácil acceso y funcionando correctamente?</th>
            <th style="background: #4472c4">P3_1_3_TXT</th>
            <th style="background: #4472c4">P3_1_2 [Mesón] ¿Los precios están ordenandos  y corresponden al producto mostrado?</th>
            <th style="background: #4472c4">P3_1_2_TXT</th>
            <th style="background: #4472c4">P3_2_1 [Mesón] ¿Los productos se aprecian ordenados? (están en el lugar que corresponde y no se mezclan entre sí)</th>
            <th style="background: #4472c4">P3_2_1_TXT</th>
            <th style="background: #4472c4">P3_2_2 [Mesón] ¿Las productos se aprecian en buen estado? (buen color y aspecto fresco (no seco), no gelatinoso o pegajoso)</th>
            <th style="background: #4472c4">P3_2_2_TXT</th>
            <th style="background: #4472c4">P3_2_3 [Mesón] ¿Hay suficiente stock de fiambres y quesos? (No se vean lugares vacíos o con falta de prodcuto)</th>
            <th style="background: #4472c4">P3_2_3_TXT</th>

            <th style="background: #4472c4">P3_2_4 [Mesón] ¿Se encuentran los siguientes productos en el sector?</th>
            <th style='background: #60eb7e'>P3_2_4 Quesos de uso diario laminados(Gauda o Mantecoso laminado)</th>
            <th style='background: #60eb7e'>P3_2_4 Quesos de uso diario trozos(Gauda o Mantecoso trozo)</th>
            <th style='background: #60eb7e'>P3_2_4 Quesos especiales para cocina(Parmesano o Reggianito)</th>
            <th style='background: #60eb7e'>P3_2_4 Quesos especiales para picoteo(Gruyere, Edam, Brie, Emmental, Ahumado, Roquefort o Mozzarella)</th>
            <th style='background: #60eb7e'>P3_2_4 Fiambres(Salame, Jamón cerdo o Jamón Ave)</th>
            
            <th style="background: #4472c4">P3_2_5 [Mesón] ¿Existe oferta de productos adicionales?</th>
            <th style="background: #4472c4">P3_2_5_TXT</th>

            <th style="background: #4472c4">P3_2_5_1</th>
            <th style='background: #60eb7e'>P3_2_5_1 Aceitunas </th>
            <th style='background: #60eb7e'>P3_2_5_1 Empanadas/Chaparritas</th>

            <th style="background: #4472c4">P3_3_1 [Mesón] Presencia de colaboradores en la vitrina de la sección </th>
            <th style="background: #4472c4">P3_3_1_TXT</th>
            <th style="background: #4472c4">P3_3_2 [Mesón] Cantidad de personal de atención</th>
            <th style="background: #4472c4">P3_3_3 [Mesón] ¿El uniforme del personal se aprecia limpio y ordenado?</th>
            <th style="background: #4472c4">P3_3_3_TXT</th>
            <th style="background: #4472c4">P3_3_4 [Mesón] ¿El personal de atención cuenta con guantes y mascarilla?</th>
            <th style="background: #4472c4">P3_3_4_TXT</th>
            <th style="background: #4472c4">P3_3_5 [Mesón] ¿El personal de atención tuvo trato amable? Saluda, ofrece asesoría (variedad, promociones) y estuvo atento a la llegada del cliente? Etc</th>
            <th style="background: #4472c4">P3_3_5_TXT</th>
            <th style="background: #4472c4">P3_1_1_1 [Envasado] ¿Se observa el sector de fiambrería limpio?</th>
            <th style="background: #4472c4">P3_1_1_1_TXT</th>
            <th style="background: #4472c4">P3_1_2_2 [Envasado] ¿Los precios están ordenandos  y corresponden al producto mostrado?</th>
            <th style="background: #4472c4">P3_1_2_2_TXT</th>
            <th style="background: #4472c4">P3_2_1_3 [Envasado] ¿Los productos se aprecian ordenados?  (están en el lugar que corresponde y no se mezclan entre sí)</th>
            <th style="background: #4472c4">P3_2_1_3_TXT</th>
            <th style="background: #4472c4">P3_2_2_4 [Envasado] ¿Las productos se aprecian en buen estado? (buen color y aspecto fresco (no seco), no gelatinoso o pegajoso)</th>
            <th style="background: #4472c4">P3_2_2_4_TXT</th>
            <th style="background: #4472c4">P3_2_3_5 [Envasado] ¿Hay suficiente stock de fiambres y quesos? (No se vean lugares vacíos o con falta de producto)</th>
            <th style="background: #4472c4">P3_2_3_5_TXT</th>

            <th style="background: #4472c4">P3_2_4_6 [Envasado] ¿Se encuentran los siguientes productos en el sector?</th>
            <th style='background: #60eb7e'>P3_2_4_6 Quesos de uso diario laminados(Gauda o Mantecoso laminado)</th>
            <th style='background: #60eb7e'>P3_2_4_6 Quesos de uso diario trozos(Gauda Trozo, Mantecoso trozo o Cabra)</th>
            <th style='background: #60eb7e'>P3_2_4_6 Quesos especiales para cocina(Granulados, Quesos fundidos, Queso crema, Reggianito, Mozzarella Bolsa o Parmesano)</th>
            <th style='background: #60eb7e'>P3_2_4_6 Quesos especiales para picoteo(Cemembert, Tablas de Queso, Provoleta, Gruyere, Edam, Brie, Emmental, Ahumado, Azul o Mozzarella)</th>
            <th style='background: #60eb7e'>P3_2_4_6 Fiambres(Salame, Jamón cerdo, Jamón Ave, Tocino, Mortadelas o Arrollados)</th>
            <th style='background: #60eb7e'>P3_2_4_6 Fiambres(Jamón cerdo, Jamón Ave)</th>
            <th style='background: #60eb7e'>P3_2_4_6 Otros Fiambres(Salame, Mortadelas o Arrolllados)</th>


            <th style="background: #4472c4">FOTO_FIAMB</th>
            <th style="background: #4472c4">P4_1_1 ¿Se observa el sector de panadería/pastelería limpio?(góndolas/piso)</th>
            <th style="background: #4472c4">P4_1_1_TXT</th>
            {{-- <th style="background: #4472c4">P4_1_2 ¿Cuántas balanzas hay disponibles?</th> --}}
            <th style="background: #4472c4">P4_1_3 ¿Se observan las balanzas limpias?</th>
            <th style="background: #4472c4">P4_1_3_TXT</th>
            <th style="background: #4472c4">P4_1_4 ¿Se observan las balanzas sin etiquetas de precio pegadas?</th>
            <th style="background: #4472c4">P4_1_4_TXT</th>
            <th style="background: #4472c4">P4_1_5 ¿Los precios/flejes están ordenandos y corresponden al producto mostrado?</th>
            <th style="background: #4472c4">P4_1_5_TXT</th>
            <th style="background: #4472c4">P4_2_1 ¿Las góndolas donde se encuentra el pan a granel se aprecian ordenadas? (el pan está en el lugar que corresponde y no se mezclan entre los diferentes tipos de panes)</th>
            <th style="background: #4472c4">P4_2_1_TXT</th>
            <th style="background: #4472c4">P4_2_2 ¿En el sector de pastelería, se aprecian los productos ordenados?</th>
            <th style="background: #4472c4">P4_2_2_TXT</th>
            <th style="background: #4472c4">P4_2_3 ¿Se siente olor a pan recién horneado?</th>
            <th style="background: #4472c4">P4_2_3_TXT</th>

            <th style="background: #4472c4">P4_2_4 ¿Se encuentran los siguientes productos en la sección?</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Marraqueta granel</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Hallulla granel</th>
            <th style='background: #60eb7e'>P4_2_4 Pan hamburguesas granel</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Hot dog granel</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Italiano granel</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Baguette granel</th>
            <th style='background: #60eb7e'>P4_2_4 Marraqueta precocida(marca lider)</th>
            <th style='background: #60eb7e'>P4_2_4 Hallula precocida(marca lider)</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Toscano granel</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Semilla Girasol(Empacado)</th>
            <th style='background: #60eb7e'>P4_2_4 Pan Semilla Calabaza(Empacado)</th>
            <th style='background: #60eb7e'>P4_2_4 Pan molde blanco XL/IDEAL</th>

            <th style="background: #4472c4">P4_2_5 ¿Se encuentran los siguientes productos en la sección de pastelería?</th>
            <th style='background: #60eb7e'>P4_2_5 Magdalena Vainilla</th>
            <th style='background: #60eb7e'>P4_2_5 Media Luna o Croissant Dulce</th>
            <th style='background: #60eb7e'>P4_2_5 Torta Selva Negra 15p</th>
            <th style='background: #60eb7e'>P4_2_5 Torta Hoja Manjar</th>

            <th style="background: #4472c4">P4_3_1 Presencia de colaboradores en la vitrina de la sección </th>
            <th style="background: #4472c4">P4_3_1_TXT</th>
            <th style="background: #4472c4">P4_3_2 Cantidad de personal de atención</th>
            <th style="background: #4472c4">P4_3_3 ¿El uniforme del personal se aprecia limpio y ordenado?</th>
            <th style="background: #4472c4">P4_3_3_TXT</th>
            <th style="background: #4472c4">P4_3_4 ¿El personal de atención cuenta con guantes y mascarilla?</th>
            <th style="background: #4472c4">P4_3_4_TXT</th>
            <th style="background: #4472c4">FOTO_PAN</th>
            {{-- <th style="background: #4472c4">OBS_GENE</th> --}}
        </tr>
    </thead>

    @foreach($datos as $dato)
        @php
            $fecha = explode('-', $dato->FEC_EVAL);
            $P1_2_4 = explode(',', $dato->P1_2_4);
            $P1_2_5 = explode(',', $dato->P1_2_5);
            $P2_1_2_6 = explode(',', $dato->P2_1_2_6);
            $P2_2_2_6 = explode(',', $dato->P2_2_2_6);
            $P2_2_2_7 = explode(',', $dato->P2_2_2_7);
            $P3_2_4 = explode(',', $dato->P3_2_4);
            $P3_2_4_6 = explode(',', $dato->P3_2_4_6);
            $P3_2_5_1 = explode(',', $dato->P3_2_5_1);
            $P4_2_4 = explode(',', $dato->P4_2_4);
            $P4_2_5 = explode(',', $dato->P4_2_5);
        @endphp
        <tr>
            <td>{{ $dato->cod_tarea }}</td>
            <td>{{ $fecha[2]  }}-{{ $fecha[1]  }}-{{ $fecha[0]  }}</td>
            <td>{{ $dato->HR_ENTRADA }}</td>
            <td>{{ $dato->HR_SALIDA }}</td>
            <td>{{ $dato->cod_sucursal }}</td>
            <td>{{ $dato->NOM_LOCAL }}</td>
            <td>Q{{ $dato->quincena }}</td>
            <td>{{ $meses[$dato->month_id] }}</td>
            <td>{{ $medicion->year }}</td>

            <td>{{ cambiaValorSiNo($dato->P1_1_1) }}</td>
            <td>{{ $dato->P1_1_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_1_2) }}</td>
            <td>{{ $dato->P1_1_2_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_1_3) }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_1_4) }}</td>
            <td>{{ $dato->P1_1_4_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_1_5) }}</td>
            <td>{{ $dato->P1_1_5_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_1_6) }}</td>
            <td>{{ $dato->P1_1_6_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_2_1) }}</td>
            <td>{{ $dato->P1_2_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_2_2) }}</td>
            <td>{{ $dato->P1_2_2_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_2_3) }}</td>
            <td>{{ $dato->P1_2_3_TXT }}</td>
            
            <td>{{ $dato->P1_2_4 }}</td>
            <td>{{ in_array('1', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('2', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('3', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('4', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('5', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('6', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('7', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('8', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('9', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('10', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('11', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('12', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('13', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('14', $P1_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('15', $P1_2_4) ? "Si" : "No"   }}</td>
            {{-- <td>{{ in_array('16', $P1_2_4) ? "Si" : "No"   }}</td> --}}
            {{-- <td>{{ in_array('17', $P1_2_4) ? "Si" : "No"   }}</td> --}}

            <td>{{ $dato->P1_2_5 }}</td>
            <td>{{ in_array('1', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('2', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('3', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('4', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('5', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('6', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('7', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('8', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('9', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('10', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('11', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('12', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('13', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('14', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('15', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('16', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('17', $P1_2_5) ? "Si" : "No"   }}</td>
            <td>{{ in_array('18', $P1_2_5) ? "Si" : "No"   }}</td>
            {{-- <td>{{ in_array('19', $P1_2_5) ? "Si" : "No"   }}</td> --}}

            <td>{{ cambiaValorSiNo($dato->P1_3_1) }}</td>
            <td>{{ $dato->P1_3_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P1_3_2) }}</td>
            <td>{{ $dato->P1_3_2_TXT }}</td>
            <td>{{ $dato->FOTO_FFVV }}</td>
            <td>{{ $dato->P0_2 == 1 ? $dato->P0_2 : ""  }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_1_1) }}</td>
            <td>{{ $dato->P2_1_1_1_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_1_2) }}</td>
            <td>{{ $dato->P2_1_1_2_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_2_1) }}</td>
            <td>{{ $dato->P2_1_2_1_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_2_2) }}</td>
            <td>{{ $dato->P2_1_2_2_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_2_3) }}</td>
            <td>{{ $dato->P2_1_2_3_T }}</td>
            
            <td>{{ $dato->P2_1_2_6 }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('1', $P2_1_2_6) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('2', $P2_1_2_6) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('3', $P2_1_2_6) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('4', $P2_1_2_6) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('5', $P2_1_2_6) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('6', $P2_1_2_6) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_1_2_6 == "" ? "" : (in_array('7', $P2_1_2_6) ? "Si" : "No") }}</td>

            <td>{{ cambiaValorSiNo($dato->P2_1_2_4) }}</td>
            <td>{{ $dato->P2_1_2_4_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_3_1) }}</td>
            <td>{{ $dato->P2_1_3_1_T }}</td>
            <td>{{ $dato->P2_1_3_2 }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_3_3) }}</td>
            <td>{{ $dato->P2_1_3_3_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_3_4) }}</td>
            <td>{{ $dato->P2_1_3_4_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_2_1_1) }}</td>
            <td>{{ $dato->P2_2_1_1_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_2_1_2) }}</td>
            <td>{{ $dato->P2_2_1_2_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_2_2_1) }}</td>
            <td>{{ $dato->P2_2_2_1_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_2_2_2) }}</td>
            <td>{{ $dato->P2_2_2_2_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_2_2_3) }}</td>
            <td>{{ $dato->P2_2_2_3_T }}</td>

            <td>{{ $dato->P2_2_2_6 }}</td>
            <td>{{ in_array('1', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('2', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('3', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('4', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('5', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('6', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('7', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('8', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('9', $P2_2_2_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('10', $P2_2_2_6) ? "Si" : "No"   }}</td>

            <td>{{ $dato->P2_2_2_7 }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('1', $P2_2_2_7) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('2', $P2_2_2_7) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('3', $P2_2_2_7) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('4', $P2_2_2_7) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('5', $P2_2_2_7) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('6', $P2_2_2_7) ? "Si" : "No") }}</td>
            <td>{{ $dato->P2_2_2_7 == "" ? "" : (in_array('7', $P2_2_2_7) ? "Si" : "No") }}</td>

            <td>{{ cambiaValorSiNo($dato->P2_2_2_4) }}</td>
            <td>{{ $dato->P2_2_2_4_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_2_2_5) }}</td>
            <td>{{ $dato->P2_2_2_5_T }}</td>
            <td>{{ cambiaValorSiNo($dato->P2_1_2_5) }}</td>
            <td>{{ $dato->P2_1_2_5_T }}</td>
            <td>{{ $dato->FOTO_CARNE }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_1_1) }}</td>
            <td>{{ $dato->P3_1_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_1_3) }}</td>
            <td>{{ $dato->P3_1_3_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_1_2) }}</td>
            <td>{{ $dato->P3_1_2_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_2_1) }}</td>
            <td>{{ $dato->P3_2_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_2_2) }}</td>
            <td>{{ $dato->P3_2_2_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_2_3) }}</td>
            <td>{{ $dato->P3_2_3_TXT }}</td>

            {{-- Isabel la Católica id=188 --}}
            @if ($dato->cod_sucursal != 188)
                <td >{{ $dato->P3_2_4 }}</td>
                <td>{{ $dato->P3_2_4 == "" ? "" : (in_array('1', $P3_2_4) ? "Si" : "No")   }}</td>
                <td>{{ $dato->P3_2_4 == "" ? "" : (in_array('2', $P3_2_4) ? "Si" : "No")   }}</td>
                <td>{{ $dato->P3_2_4 == "" ? "" : ($dato->month_id >= 10 ? "-" : (in_array('3', $P3_2_4) ? "Si" : "No"))   }}</td>
                <td>{{ $dato->P3_2_4 == "" ? "" : (in_array('4', $P3_2_4) ? "Si" : "No")   }}</td>
                <td>{{ $dato->P3_2_4 == "" ? "" : (in_array('5', $P3_2_4) ? "Si" : "No")   }}</td>
            @else
                <td >*{{ $dato->P3_2_4 == '1,2,3' ? 5 : $dato->P3_2_4 }}</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>{{ $dato->P3_2_4 == '1,2,3' || 5 ? "Si" : "No"  }}</td>
            @endif
                
                
            
            
            



            <td>{{ cambiaValorSiNo($dato->P3_2_5) }}</td>
            <td>{{ $dato->P3_2_5_TXT }}</td>

            <td>{{ $dato->P3_2_5_1 }}</td>
            <td>{{ $dato->P3_2_5_1 == "" ? "" : (in_array('1', $P3_2_5_1) ? "Si" : "No")   }}</td>
            <td>{{ $dato->P3_2_5_1 == "" ? "" : (in_array('2', $P3_2_5_1) ? "Si" : "No")   }}</td>



            <td>{{ cambiaValorSiNo($dato->P3_3_1) }}</td>
            <td>{{ $dato->P3_3_1_TXT }}</td>
            <td>{{ $dato->P3_3_2 }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_3_3) }}</td>
            <td>{{ $dato->P3_3_3_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_3_4) }}</td>
            <td>{{ $dato->P3_3_4_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_3_5) }}</td>
            <td>{{ $dato->P3_3_5_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_1_1_1) }}</td>
            <td>{{ $dato->P3_1_1_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_1_2_2) }}</td>
            <td>{{ $dato->P3_1_2_2_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_2_1_3) }}</td>
            <td>{{ $dato->P3_2_1_3_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_2_2_4) }}</td>
            <td>{{ $dato->P3_2_2_4_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P3_2_3_5) }}</td>
            <td>{{ $dato->P3_2_3_5_TXT }}</td>

            <td>{{ $dato->P3_2_4_6 }}</td>
            <td>{{ in_array('1', $P3_2_4_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('2', $P3_2_4_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('3', $P3_2_4_6) ? "Si" : "No"   }}</td>
            <td>{{ in_array('4', $P3_2_4_6) ? "Si" : "No"   }}</td>

            @if ($dato->month_id < 9)
                <td>{{ in_array('5', $P3_2_4_6) ? "Si" : "No"   }}</td>
            @else
                <td>-</td>
            @endif
            @if ($dato->month_id >= 9)
                <td>{{ in_array('6', $P3_2_4_6) ? "Si" : "No"   }}</td>
                <td>{{ in_array('7', $P3_2_4_6) ? "Si" : "No"   }}</td>
            @else
                <td>-</td>
                <td>-</td>
            @endif
            


            <td>{{ $dato->FOTO_FIAMB }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_1_1) }}</td>
            <td>{{ $dato->P4_1_1_TXT }}</td>
            {{-- <td>{{ $dato->P4_1_2 }}</td> --}}
            <td>{{ cambiaValorSiNo($dato->P4_1_3) }}</td>
            <td>{{ $dato->P4_1_3_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_1_4) }}</td>
            <td>{{ $dato->P4_1_4_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_1_5) }}</td>
            <td>{{ $dato->P4_1_5_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_2_1) }}</td>
            <td>{{ $dato->P4_2_1_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_2_2) }}</td>
            <td>{{ $dato->P4_2_2_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_2_3) }}</td>
            <td>{{ $dato->P4_2_3_TXT }}</td>

            <td>{{ $dato->P4_2_4 }}</td>
            <td>{{ in_array('1', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('2', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('3', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('4', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('5', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('6', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('7', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('8', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('9', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('10', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('11', $P4_2_4) ? "Si" : "No"   }}</td>
            <td>{{ in_array('12', $P4_2_4) ? "Si" : "No"   }}</td>


            <td>{{ $dato->P4_2_5 }}</td>
            <td>{{ $dato->P4_2_5 == "" ? "" : (in_array('1', $P4_2_5) ? "Si" : "No")   }}</td>
            <td>{{ $dato->P4_2_5 == "" ? "" : (in_array('2', $P4_2_5) ? "Si" : "No")   }}</td>
            <td>{{ $dato->P4_2_5 == "" ? "" : (in_array('3', $P4_2_5) ? "Si" : "No")   }}</td>
            <td>{{ $dato->P4_2_5 == "" ? "" : (in_array('4', $P4_2_5) ? "Si" : "No")   }}</td>



            <td>{{ cambiaValorSiNo($dato->P4_3_1) }}</td>
            <td>{{ $dato->P4_3_1_TXT }}</td>
            <td>{{ $dato->P4_3_2 }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_3_3) }}</td>
            <td>{{ $dato->P4_3_3_TXT }}</td>
            <td>{{ cambiaValorSiNo($dato->P4_3_4) }}</td>
            <td>{{ $dato->P4_3_4_TXT }}</td>
            <td>{{ $dato->FOTO_PAN }}</td>
            {{-- <td>{{ $dato->OBS_GENE }}</td> --}}





        </tr>
    @endforeach

</table>