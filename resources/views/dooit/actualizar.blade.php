@extends('layouts.admin')

@section('content')



<div class="badge bg-primary text-white rounded-pill">Full-time</div>

    <div class="container-xl mt-4">
        <div class="card mb-4">
            <div class="card-header">Actualización datos desde Dooit</div>
            <div class="card-body ">
                <!-- Billing history table-->
                <div class="table-responsive table-billing-history">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th class="border-gray-200" scope="col">Tabla</th>
                                <th class="border-gray-200" scope="col">Registros</th>
                                <th class="border-gray-200" scope="col">Última actualización</th>
                                <th class="border-gray-200" scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Cuestionario</td>
                                <td>55</td>
                                <td>29/07/2021</td>
                                <td>
                                    <button class="badge bg-light">Actualizar</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Pregunta</td>
                                <td>55</td>
                                <td>29/07/2021</td>
                                <td>
                                    <button class="badge bg-light">Actualizar</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Alernativas</td>
                                <td>55</td>
                                <td>29/07/2021</td>
                                <td>
                                    <button class="badge bg-light">Actualizar</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Sucursal</td>
                                <td>55</td>
                                <td>29/07/2021</td>
                                <td>
                                    <button class="badge bg-light">Actualizar</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Tarea</td>
                                <td>55</td>
                                <td>29/07/2021</td>
                                <td>
                                    <button class="badge bg-light">Actualizar</button>
                                </td>
                            </tr>
                            
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection