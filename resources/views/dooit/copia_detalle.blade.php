@extends('layouts.admin')

@section('content')

    <div class="container-xl mt-4">
        <div class="card mb-4">
            <div class="card-header">Copia los datos a detalle</div>
            <div class="card-body ">
                <!-- Billing history table-->
                <form method="POST" action="{{ route('datos.detalle.copia') }}" class="user">
                    @csrf
                    <button type="submit" class="btn btn-primary">Procesar</button>
                </form>
            </div>
        </div>
    </div>
@endsection