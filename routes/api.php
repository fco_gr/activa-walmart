<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::resource('user', API\V1\UserController::class)->middleware('auth:api');

Route::middleware('auth:api')->group(function () {
    Route::post('dashboard', 'API\V1\DashboardController@getResumen');
    Route::post('dashboard_total', 'API\V1\DashboardController@getTotales');
    Route::post('ranking_local', 'API\V1\RankingLocalController@getRanking');
    Route::post('benchmark', 'API\V1\BenchmarkController@index');
    
    Route::post('variedad_por_local_producto', 'API\V1\VariedadPorLocalController@getPreguntas');

    Route::get('medicion', 'API\V1\MedicionController@index');
    Route::get('medicion/list/', 'API\V1\MedicionController@list');
    
    Route::post('evaluacion_list', 'API\V1\EvaluacionController@getListByMedicion');
    Route::post('evaluacion', 'API\V1\EvaluacionController@getSucursalByCodMedicion');
    
    Route::post('descarga_db', 'API\V1\BaseDeDatosController@DescargaBase');

    

});

// Route::namespace('App\\Http\\Controllers\\API\V1')->group(function () {
    // Route::get('profile', 'ProfileController@profile');
    // Route::put('profile', 'ProfileController@updateProfile');
    // Route::post('change-password', 'ProfileController@changePassword');
    // Route::get('formato/list', 'FormatoController@list');
    // Route::get('category/list', 'CategoryController@list');
    // Route::post('product/upload', 'ProductController@upload');

    // Route::apiResources([
    //     'user' => 'UserController',
    //     'product' => 'ProductController',
    //     'category' => 'CategoryController',
    //     'tag' => 'TagController',
    // ]);
// });



