<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/phpinfo', function () {
//     echo phpinfo();
// });

Route::get('/', function () {
    return redirect('dashboard');
});

Route::get('users', 'UserController')->name('user.index');
// Route::get('developer', 'DeveloperController')->name('user.index');

Auth::routes(['register' => false]);



Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/punto_contacto', 'PuntoContactoController@index')->name('punto.contacto');
Route::get('/variedadad_por_seccion', 'VariedadSeccionController@index')->name('variedad.por.seccion');
Route::get('/variedadad_por_local', 'VariedadPorLocalController')->name('variedad.por.local');
Route::get('/evaluaciones', 'EvaluacionController')->name('evaluacion.index');

// Route::get('/punto_contacto', function () {
//     return view('punto_contacto.index');
// })->name('punto.contacto');

// Route::get('/variedadad_por_seccion', function () {
//     return view('variedad_por_seccion.index');
// })->name('variedad.por.seccion');

// Route::get('/ficha', function () {
//     return view('ficha.index');
// })->name('ficha.index');

Route::get('/ranking_local', 'RankingLocalController')->name('ranking_local');
Route::get('/benchmark', 'BenchmarkController@index')->name('benchmark');
Route::get('/ficha', 'FichaController@index')->name('ficha.index');
Route::get('/base', 'BaseDeDatosController@index')->name('base.index');
Route::get('/base/{medicion}/{formato}', 'BaseDeDatosController@export')->name('base.export');



Route::get('/dooit/actualiza', 'DooitController@index')->name('dooit');
Route::get('/dooit/datos_detalle', 'DatosDetalleController@index')->name('datos.detalle');
Route::post('/dooit/datos_detalle', 'DatosDetalleController@copiaDatosDetalle')->name('datos.detalle.copia');

Route::get('/sync_cuestionarios', 'Dooit\DooitController@getCuestionarios');  // Actualiza la información de los cuestionarios 
Route::get('/sync_secciones', 'Dooit\DooitController@getSecciones');
//Route::get('/sync_preguntas', 'Dooit\DooitController@getPreguntas');
Route::get('/sync_alternativas', 'Dooit\DooitController@getAlternativas');
//Route::get('/sync_sucursales', 'Dooit\DooitController@getSucursales');
Route::get('/sync_tareas', 'Dooit\DooitController@getTareas'); // Carga y actualiza Tareas y Datos

Route::get('formato/list', 'API\V1\FormatoController@list');
Route::get('sucursal/list', 'API\V1\SucursalController@list');
Route::get('seccion/list/', 'API\V1\SeccionController@list');
Route::get('dimension/list/', 'API\V1\DimensionController@list');


Route::post('post_punto_contacto', 'API\V1\PuntoContactoController@getFilter');
Route::post('post_punto_contacto2', 'API\V1\PuntoContactoController@getFilter2');
Route::post('post_punto_totales', 'API\V1\PuntoContactoController@getFilterGraficoTotales');

Route::post('post_variedad_por_seccion', 'API\V1\VariedadPorSeccionController@getByPregunta');
Route::post('post_variedad_por_pregunta', 'API\V1\VariedadPorSeccionController@getVariedadByPregunta');
