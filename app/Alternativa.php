<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alternativa extends Model
{
    protected $fillable = [
        'id_alternativa', 'cod_estudio', 'cod_cuestionario', 'cod_pregunta',
        'cod_alternativa', 'literal_alternativa', 'orden', 'estado'
    ];
}
