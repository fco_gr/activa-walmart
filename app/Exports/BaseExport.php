<?php

namespace App\Exports;
use App\Medicion;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class BaseExport implements FromView, WithColumnFormatting
{
    public function __construct(string $medicion, int $formato)
    {
        $this->medicion = $medicion;
        $this->pauta = $formato;
    }

    public function columnFormats(): array
    {

        if ($this->pauta != 3){
            $formato_columnas = [
                'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                'J' => NumberFormat::FORMAT_PERCENTAGE,
                'L' => NumberFormat::FORMAT_PERCENTAGE,
                'N' => NumberFormat::FORMAT_PERCENTAGE,
                'O' => NumberFormat::FORMAT_PERCENTAGE,
                'Q' => NumberFormat::FORMAT_PERCENTAGE,
                'S' => NumberFormat::FORMAT_PERCENTAGE,
                'U' => NumberFormat::FORMAT_PERCENTAGE,
                'W' => NumberFormat::FORMAT_PERCENTAGE,
                'Y' => NumberFormat::FORMAT_PERCENTAGE,
                'BJ' => NumberFormat::FORMAT_PERCENTAGE,
                'BK' => NumberFormat::FORMAT_PERCENTAGE,
                'BP' => NumberFormat::FORMAT_PERCENTAGE,
                'BR' => NumberFormat::FORMAT_PERCENTAGE,
                'BT' => NumberFormat::FORMAT_PERCENTAGE,
                'BV' => NumberFormat::FORMAT_PERCENTAGE,
                'BX' => NumberFormat::FORMAT_PERCENTAGE,
                'CH' => NumberFormat::FORMAT_PERCENTAGE,
                'CJ' => NumberFormat::FORMAT_PERCENTAGE,
                'CM' => NumberFormat::FORMAT_PERCENTAGE,
                'CO' => NumberFormat::FORMAT_PERCENTAGE,
                'CQ' => NumberFormat::FORMAT_PERCENTAGE,
                'CS' => NumberFormat::FORMAT_PERCENTAGE,
                'CU' => NumberFormat::FORMAT_PERCENTAGE,
                'CW' => NumberFormat::FORMAT_PERCENTAGE,
                'CY' => NumberFormat::FORMAT_PERCENTAGE,
                'DT' => NumberFormat::FORMAT_PERCENTAGE,
                'DV' => NumberFormat::FORMAT_PERCENTAGE,
                'DX' => NumberFormat::FORMAT_PERCENTAGE,
                'EA' => NumberFormat::FORMAT_PERCENTAGE,
                'EC' => NumberFormat::FORMAT_PERCENTAGE,
                'EE' => NumberFormat::FORMAT_PERCENTAGE,
                'EG' => NumberFormat::FORMAT_PERCENTAGE,
                'EI' => NumberFormat::FORMAT_PERCENTAGE,
                'EK' => NumberFormat::FORMAT_PERCENTAGE,
                'ES' => NumberFormat::FORMAT_PERCENTAGE,
                'EX' => NumberFormat::FORMAT_PERCENTAGE,
                'FA' => NumberFormat::FORMAT_PERCENTAGE,
                'FC' => NumberFormat::FORMAT_PERCENTAGE,
                'FE' => NumberFormat::FORMAT_PERCENTAGE,
                'FG' => NumberFormat::FORMAT_PERCENTAGE,
                'FI' => NumberFormat::FORMAT_PERCENTAGE,
                'FK' => NumberFormat::FORMAT_PERCENTAGE,
                'FM' => NumberFormat::FORMAT_PERCENTAGE,
                'FO' => NumberFormat::FORMAT_PERCENTAGE,
                'FZ' => NumberFormat::FORMAT_PERCENTAGE,
                'GB' => NumberFormat::FORMAT_PERCENTAGE,
                'GD' => NumberFormat::FORMAT_PERCENTAGE,
                'GF' => NumberFormat::FORMAT_PERCENTAGE,
                'GH' => NumberFormat::FORMAT_PERCENTAGE,
                'GJ' => NumberFormat::FORMAT_PERCENTAGE,
                'GL' => NumberFormat::FORMAT_PERCENTAGE,
                'HF' => NumberFormat::FORMAT_PERCENTAGE,
                'HI' => NumberFormat::FORMAT_PERCENTAGE,
                'HK' => NumberFormat::FORMAT_PERCENTAGE,
            ];
        }
        else {
            $formato_columnas = [
                'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                'J' => NumberFormat::FORMAT_PERCENTAGE,
                'L' => NumberFormat::FORMAT_PERCENTAGE,
                'N' => NumberFormat::FORMAT_PERCENTAGE,
                'O' => NumberFormat::FORMAT_PERCENTAGE,
                'Q' => NumberFormat::FORMAT_PERCENTAGE,
                'S' => NumberFormat::FORMAT_PERCENTAGE,
                'U' => NumberFormat::FORMAT_PERCENTAGE,
                'W' => NumberFormat::FORMAT_PERCENTAGE,
                'Y' => NumberFormat::FORMAT_PERCENTAGE,
                'AT' => NumberFormat::FORMAT_PERCENTAGE,
                'AV' => NumberFormat::FORMAT_PERCENTAGE,
                'AY' => NumberFormat::FORMAT_PERCENTAGE,
                'BA' => NumberFormat::FORMAT_PERCENTAGE,
                'BB' => NumberFormat::FORMAT_PERCENTAGE,
                'BC' => NumberFormat::FORMAT_PERCENTAGE,
                'BE' => NumberFormat::FORMAT_PERCENTAGE,
                'BG' => NumberFormat::FORMAT_PERCENTAGE,
                'CG' => NumberFormat::FORMAT_PERCENTAGE,
                'CN' => NumberFormat::FORMAT_PERCENTAGE,
                'CK' => NumberFormat::FORMAT_PERCENTAGE,
                'CP' => NumberFormat::FORMAT_PERCENTAGE,
                'CR' => NumberFormat::FORMAT_PERCENTAGE,
                'CT' => NumberFormat::FORMAT_PERCENTAGE,
                'CV' => NumberFormat::FORMAT_PERCENTAGE,
                'DI' => NumberFormat::FORMAT_PERCENTAGE,

                'DK' => NumberFormat::FORMAT_PERCENTAGE,
                'DM' => NumberFormat::FORMAT_PERCENTAGE,
                'DO' => NumberFormat::FORMAT_PERCENTAGE,
                'DQ' => NumberFormat::FORMAT_PERCENTAGE,
                'DS' => NumberFormat::FORMAT_PERCENTAGE,
                'DU' => NumberFormat::FORMAT_PERCENTAGE,
                'EO' => NumberFormat::FORMAT_PERCENTAGE,
                'ER' => NumberFormat::FORMAT_PERCENTAGE,
                'ET' => NumberFormat::FORMAT_PERCENTAGE,
            ];
        }

        return $formato_columnas;
    }

    public function view(): View
    {
        $sql = "select datos.*, tareas.cod_sucursal, tareas.nombre as NOM_LOCAL, medicion_tarea.medicion_id as medicion_id, quincena, month_id  ";
        $sql .= "from datos, tareas, medicion_tarea, formatos_sucursals, medicions  ";
        $sql .= "where datos.cod_tarea = tareas.cod_tarea and tareas.cod_tarea = medicion_tarea.cod_tarea AND medicion_tarea.medicion_id = medicions.id and ";
        $sql .= "tareas.cod_sucursal = formatos_sucursals.cod_sucursal  ";
        $sql .= "and medicion_tarea.medicion_id = $this->medicion and formatos_sucursals.formato_id = $this->pauta";
        // $sql .= "and formatos_sucursals.formato_id = $this->pauta";

        // dd($sql);
        $datos = DB::select($sql);
        $datosMedicion = Medicion::find($this->medicion);

        $vista = 'exports.base';
        if ($this->pauta == 3){
            $vista = 'exports.base_sba';
        }

        return view($vista, [
            'datos' => $datos,
            'medicion' => $datosMedicion,
        ]);
    }
}
