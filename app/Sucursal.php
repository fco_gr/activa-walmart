<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $fillable = ['id', 'cod_sucursal', 'cod_estudio', 'nombre_sucursal', 'cod_pais_nivel', 'direccion', 'lat', 'lon', 'ubicacion', 'adicionales', 'estado', 'created_at', 'updated_at'];
}
