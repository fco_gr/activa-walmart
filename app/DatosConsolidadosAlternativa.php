<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosConsolidadosAlternativa extends Model
{
    protected $fillable = [
        'cod_estudio', 'cod_tarea','cod_pregunta', 'cod_interno', 'respuesta', 'puntaje', 'puntaje_total'
    ];
}
