<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuestionario extends Model
{
    protected $fillable = ['id', 'cod_cuestionario', 'cod_estudio', 'titulo_cuestionario', 'descripcion', 'estado', 'fecha_creacion', 'autor', 'pasos_completos', 'created_at', 'updated_at'];
}
