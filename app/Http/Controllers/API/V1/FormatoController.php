<?php

namespace App\Http\Controllers\API\V1;

use App\Formato;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FormatoController extends BaseController
{

    protected $formato = '';


    public function __construct(Formato $formato)
    {
        $this->middleware('auth');
        $this->formato = $formato;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formatos = Formato::all();
    }

    public function list()
    {
        // return 12;
        $formatos = $this->formato->pluck('descripcion', 'id');
        return $this->sendResponse($formatos, 'Listado Formato');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Formato  $formato
     * @return \Illuminate\Http\Response
     */
    public function show(Formato $formato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Formato  $formato
     * @return \Illuminate\Http\Response
     */
    public function edit(Formato $formato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Formato  $formato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formato $formato)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Formato  $formato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formato $formato)
    {
        //
    }
}
