<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends BaseController
{

    private function getWhere($request, $opciones = null){
        //dd($request->filtro);
        $where = '';


        if ($opciones['medicion_id'] == true){
            if($request->filtro['medicion_id'] != 0){
                $where .= " and medicions.id = " . $request->filtro['medicion_id'] . " ";
            }
        }

        if ($opciones['seccion_id'] == true){
            if($request->filtro['seccion_id'] != 0){
                $where .= " and seccions.cod_seccion = " . $request->filtro['seccion_id'] . " ";
            }
        }

        if($request->filtro['sucursal_id'] != 0){
            $where .= " and sucursals.cod_sucursal = " . $request->filtro['sucursal_id'] . " ";
        }

        if($request->filtro['formato_id'] != 0){
            $where .= " and formatos.id = " . $request->filtro['formato_id'] . " ";
        }

        if($request->filtro['dimension_id'] != 0){
            $where .= " and dimensions.id = " . $request->filtro['dimension_id'] . " ";
        }

        return $where;


    }


    public function getTotales(Request $request){
        // dd($request);
        
        $where = $this->getWhere($request, ['seccion_id' => true, 'medicion_id' => true]);

        // dd($where);

        $sql = "select 0 as id, 'Lider Total' as descripcion, avg(porc) as porc from ( ";
        $sql .= "select cod_seccion, titulo_seccion, avg(porc) as porc from ( ";
        $sql .= "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from (  ";
        $sql .= "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from ( ";
        $sql .= "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from ( ";
        $sql .= "select formatos.id as formato_id, formatos.descripcion as formato_descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id,   ";
        $sql .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc  ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions   ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND   ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and   ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and   ";
        $sql .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno and formatos.id in (1, 2)                                    $where  ";
        $sql .= "group by formatos.id, formatos.descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id   ";
        $sql .= "order by cod_seccion, dimension_id ";
        $sql .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta ";
        $sql .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion ";
        $sql .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion ";
        $sql .= ") as datos group by cod_seccion, titulo_seccion ";
        $sql .= ") as datos ";
        $sql .= "union ";
        $sql .= "select formato_id as id, formato_descripcion as descripcion, avg(porc) as porc from ( ";
        $sql .= "select formato_id, formato_descripcion, cod_seccion, titulo_seccion, avg(porc) as porc from ( ";
        $sql .= "select formato_id, formato_descripcion, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from (  ";
        $sql .= "select formato_id, formato_descripcion, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from ( ";
        $sql .= "select formato_id, formato_descripcion, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from ( ";
        $sql .= "select formatos.id as formato_id, formatos.descripcion as formato_descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id,   ";
        $sql .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc  ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions   ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND   ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and   ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and   ";
        $sql .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno                                  $where    ";
        $sql .= "group by formatos.id, formatos.descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id   ";
        $sql .= "order by cod_seccion, dimension_id ";
        $sql .= ") as datos group by formato_id, formato_descripcion, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta ";
        $sql .= ") as datos group by formato_id, formato_descripcion, cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion ";
        $sql .= ") as datos group by formato_id, formato_descripcion, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion ";
        $sql .= ") as datos group by formato_id, formato_descripcion, cod_seccion, titulo_seccion ";
        $sql .= ") as datos group by formato_id, formato_descripcion";

        // dd($sql);

        $decimal = 2;
        
        $resultadoTotalFormato = [];
        $datos = DB::select($sql);
        foreach ($datos as $dato){
            $resultadoTotalFormato['formato_'.$dato->id] = round($dato->porc, $decimal);
        }

        return $this->sendResponse($resultadoTotalFormato, 'Total por formato');

    }

    // Gráfico
    public function getResumen(Request $request){
        
        $where = $this->getWhere($request, ['seccion_id' => true, 'medicion_id' => false]);
        // $where = '';

        // if($request->filtro['medicion_id'] != 0){
        //     $where .= " and medicions.id = " . $request->filtro['medicion_id'] . " ";
        // }

        // if($request->filtro['seccion_id'] != 0){
        //     $where .= " and seccions.cod_seccion = " . $request->filtro['seccion_id'] . " ";
        // }

        // if($request->filtro['sucursal_id'] != 0){
        //     $where .= " and sucursals.cod_sucursal = " . $request->filtro['sucursal_id'] . " ";
        // }

        // if($request->filtro['formato_id'] != 0){
        //     $where .= " and formatos.id = " . $request->filtro['formato_id'] . " ";
        // }

        // if($request->filtro['dimension_id'] != 0){
        //     $where .= " and dimensions.id = " . $request->filtro['dimension_id'] . " ";
        // }

        $sql = "select 0 as id, 'Lider Total' as descripcion, medicion_id, descripcion_corto, avg(porc) as porc from ( ";
        $sql .= "select medicion_id, descripcion_corto, cod_seccion, titulo_seccion, avg(porc) as porc from ( ";
        $sql .= "select medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from (  ";
        $sql .= "select medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from ( ";
        $sql .= "select medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from ( ";
        $sql .= "select formatos.id as formato_id, formatos.descripcion as formato_descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,  ";
        $sql .= "dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id, medicions.descripcion_corto,  ";
        $sql .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc  ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions   ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND   ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and   ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and   ";
        $sql .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno and formatos.id in (1, 2)                                  $where   ";
        $sql .= "group by formatos.id, formatos.descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,  ";
        $sql .= "dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id, medicions.descripcion_corto   ";
        $sql .= "order by cod_seccion, dimension_id ";
        $sql .= ") as datos group by medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta ";
        $sql .= ") as datos group by medicion_id, descripcion_corto, cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion ";
        $sql .= ") as datos group by medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion ";
        $sql .= ") as datos group by medicion_id, descripcion_corto, cod_seccion, titulo_seccion ";
        $sql .= ") as datos group by medicion_id, descripcion_corto ";
        $sql .= "union ";
        $sql .= "select formato_id as id, formato_descripcion as descripcion, medicion_id, descripcion_corto, avg(porc) as porc from ( ";
        $sql .= "select formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion, avg(porc) as porc from ( ";
        $sql .= "select formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from (  ";
        $sql .= "select formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from ( ";
        $sql .= "select formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from ( ";
        $sql .= "select formatos.id as formato_id, formatos.descripcion as formato_descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,  ";
        $sql .= "dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id, medicions.descripcion_corto,  ";
        $sql .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc  ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions   ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND   ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and   ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and   ";
        $sql .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno                                   $where   ";
        $sql .= "group by formatos.id, formatos.descripcion, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,  ";
        $sql .= "dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id, medicions.descripcion_corto   ";
        $sql .= "order by cod_seccion, dimension_id ";
        $sql .= ") as datos group by formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta ";
        $sql .= ") as datos group by formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion ";
        $sql .= ") as datos group by formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion ";
        $sql .= ") as datos group by formato_id, formato_descripcion, medicion_id, descripcion_corto, cod_seccion, titulo_seccion ";
        $sql .= ") as datos group by formato_id, formato_descripcion, medicion_id, descripcion_corto ";
        $sql .= "order by id, medicion_id";

        
        // dd($sql);
        $resultadoPorFormato = [];
        $datos = DB::select($sql);
        $primera = true;
        $valorResta = 0;
        $decimal = 2;
        foreach ($datos as $dato){
            if ($primera){
                $valorResta = $dato->medicion_id;
                $primera = false;
            }
            $resultadoPorFormato['formato_' . $dato->id][$dato->medicion_id-$valorResta] = number_format($dato->porc, $decimal, '.', ',');
        }

        // dd($resultadoPorFormato);

        return $this->sendResponse($resultadoPorFormato, 'Listado dimensiones');


    }

}
