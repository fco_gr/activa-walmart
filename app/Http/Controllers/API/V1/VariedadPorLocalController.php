<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class VariedadPorLocalController extends BaseController
{
    public function getPreguntas(Request $request){

        // dd($request->all());
        $pregunta = $request->cod_interno;
        $formato = $request->filtro ? $request->filtro['formato_id'] : null;
        $medicion = $request->filtro ? $request->filtro['medicion_id'] : null;
        // dd($formato);

        $largoPregunta = strlen($pregunta) + 3;

        $sql = "";
        $sql .= "select num, todas.cod_sucursal, todas.nombre_sucursal, todas.cod_interno, todas.literal_pregunta, existe from  ";
        $sql .= "( ";
        $sql .= "select * from  ";
        $sql .= "( ";
        $sql .= "select (@row_number:=@row_number + 1) AS num, sucursals.cod_sucursal, nombre_sucursal from sucursals, formatos_sucursals, (SELECT @row_number:=0) AS t where sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formato_id=$formato ";
        $sql .= ") as sucursal,  ";
        $sql .= "( ";
        $sql .= "select pregunta_alternativas.cod_interno, pregunta_alternativas.literal_pregunta ";
        $sql .= "from datos_consolidados_alternativas, tareas, pregunta_alternativas, medicion_tarea, medicions, sucursals, formatos_sucursals ";
        $sql .= "where datos_consolidados_alternativas.cod_tarea = tareas.cod_tarea and datos_consolidados_alternativas.cod_interno = pregunta_alternativas.cod_interno and  ";
        $sql .= "tareas.cod_tarea = medicion_tarea.cod_tarea and medicion_tarea.medicion_id = medicions.id and tareas.cod_sucursal = sucursals.cod_sucursal and sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and   ";
        $sql .= "pregunta_alternativas.cod_interno like '" . $pregunta . "_%'  and formato_id=$formato  and LENGTH (pregunta_alternativas.cod_interno) <= $largoPregunta  ";
        $sql .= "group by  pregunta_alternativas.cod_interno, pregunta_alternativas.literal_pregunta ";
        $sql .= ") as pregunta_alternativas order by cod_sucursal, num, literal_pregunta ";
        $sql .= ") as todas left join  ";
        $sql .= "( ";
        $sql .= "select tareas.cod_sucursal, pregunta_alternativas.cod_interno, datos_consolidados_alternativas.puntaje as existe  ";
        $sql .= "from datos_consolidados_alternativas, tareas, pregunta_alternativas, medicion_tarea, medicions, sucursals, formatos_sucursals ";
        $sql .= "where datos_consolidados_alternativas.cod_tarea = tareas.cod_tarea and datos_consolidados_alternativas.cod_interno = pregunta_alternativas.cod_interno and  ";
        $sql .= "tareas.cod_tarea = medicion_tarea.cod_tarea and medicion_tarea.medicion_id = medicions.id and ";
        $sql .= "tareas.cod_sucursal = sucursals.cod_sucursal and sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and ";
        $sql .= "pregunta_alternativas.cod_interno like '" . $pregunta . "_%' and medicions.id = $medicion and formato_id=$formato and LENGTH (pregunta_alternativas.cod_interno) <= $largoPregunta ";
        $sql .= "order by pregunta_alternativas.literal_pregunta ";
        $sql .= ") as base on todas.cod_sucursal=base.cod_sucursal and todas.cod_interno=base.cod_interno";

        // LENGTH (pregunta_alternativas.cod_interno)

        // dd($sql);

        $preguntas = DB::select($sql);

        $datos = [];
        $sucursales = [];
        foreach ($preguntas as $pregunta){
            // dd($pregunta);
            $datos[$pregunta->cod_interno]['literal_pregunta'] = $pregunta->literal_pregunta;
            $datos[$pregunta->cod_interno]['datos'][$pregunta->num] = $pregunta;
            $sucursales[$pregunta->num] = $pregunta->nombre_sucursal;
        }
        // dd($preguntas);

        return $this->sendResponse(
            [
                'preguntas' => $datos,
                'locales' => $sucursales
            ], 'Preguntas Disponibilidad');

    }

}
