<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Sucursal;
use Illuminate\Http\Request;

class SucursalController extends BaseController
{
    protected $sucursal = '';

    public function __construct(Sucursal $sucursal)
    {
        $this->middleware('auth');
        $this->sucursal = $sucursal;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucursales = Sucursal::paginate(10);
    }

    public function list()
    {

        $formato = \request()->get('formato') !== null ? \request()->get('formato') : '';
        // dd($formato);

        $formatoT = 'Lider';
        if ($formato == 1){
            $formatoT = "Lider";
        }
        else if ($formato == 2){
            $formatoT = "Express";
        }
        else if ($formato == 3){
            $formatoT = "SBA";
        }


        $sucursals = Sucursal::select(['nombre_sucursal', 'cod_sucursal'])
            ->where('nombre_sucursal', 'like', $formatoT.'%')
            ->orderBy('nombre_sucursal')
            ->pluck('nombre_sucursal', 'cod_sucursal');
        // dd($sucursals);
        // $sucursals = $this->sucursal->pluck('nombre_sucursal', 'cod_sucursal');
        return $this->sendResponse($sucursals, 'Listado Sucursales');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function show(Sucursal $sucursal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function edit(Sucursal $sucursal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sucursal $sucursal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sucursal $sucursal)
    {
        //
    }
}
