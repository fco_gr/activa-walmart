<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EvaluacionController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function getWhere($request, $opciones = null){
        //dd($request->filtro);
        $where = '';


        if ($opciones['medicion_id'] == true){
            if($request->filtro['medicion_id'] != 0){
                $where .= " and medicions.id = " . $request->filtro['medicion_id'] . " ";
            }
        }

        if ($opciones['seccion_id'] == true){
            if($request->filtro['seccion_id'] != 0){
                $where .= " and seccions.cod_seccion = " . $request->filtro['seccion_id'] . " ";
            }
        }

        if($request->filtro['sucursal_id'] != 0){
            $where .= " and sucursals.cod_sucursal = " . $request->filtro['sucursal_id'] . " ";
        }

        if($request->filtro['formato_id'] != 0){
            $where .= " and formatos.id = " . $request->filtro['formato_id'] . " ";
        }

        if($request->filtro['dimension_id'] != 0){
            $where .= " and dimensions.id = " . $request->filtro['dimension_id'] . " ";
        }

        return $where;


    }

    public function getSucursalByCodMedicion(Request $request){
        // dd($request['filtro']);
        // dd($request->all());

        $sqlWhere = "";
        $decimales = 1;


        if ($request['filtro']['medicion_id'] != 0){
            // $where .= " and medicions.id = $medicion_id ";
            $sqlWhere .= " and medicions.id = " . $request['filtro']['medicion_id'] . " ";
        }

        
        if ($request['local']['cod_sucursal'] != 0){
            $sqlWhere .= "and sucursals.cod_sucursal = " . $request['local']['cod_sucursal'] . " ";
        }
        

        $arrarSalida = [];
        $grafico = [];
        $puntajeTotal = 0;


        // $sqlWhere = "and formatos.id = 3 and medicions.id = 1";

        $sqlBase = "select seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id,  ";
        $sqlBase .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc ";
        $sqlBase .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions  ";
        $sqlBase .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sqlBase .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sqlBase .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sqlBase .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno              $sqlWhere               ";
        $sqlBase .= "group by seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id  ";
        $sqlBase .= "order by cod_seccion, dimension_id";

        


        $sqlPregunta = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from (";
        $sqlPregunta .= " $sqlBase ";
        $sqlPregunta .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta";


        $preguntaDatos = DB::select($sqlPregunta);
        foreach ($preguntaDatos as $key => $preguntaDato){
            $arrarSalida['S_'.$preguntaDato->cod_seccion]['sub_seccion']['ss_'.$preguntaDato->sub_seccion_id]['dimension']['d_'.$preguntaDato->dimension_id]['preguntas']['p_'.$preguntaDato->cod_interno]['dato']  = $preguntaDato;
        }

        // ['sub_seccion']['ss_'.$preguntaDato->sub_seccion_id]
        // -- Evolutivo
        
        $sqlPreguntaM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, medicion_id, avg(porc) as porc from (";
        $sqlPreguntaM .= " $sqlBase ";
        $sqlPreguntaM .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, medicion_id";
        
        
        $preguntaMDatos = DB::select($sqlPreguntaM);
        foreach ($preguntaMDatos as $key => $preguntaMDato){
            $arrarSalida['S_'.$preguntaMDato->cod_seccion]['sub_seccion']['ss_'.$preguntaMDato->sub_seccion_id]['dimension']['d_'.$preguntaMDato->dimension_id]['preguntas']['p_'.$preguntaMDato->cod_interno]['medicion'][$preguntaMDato->medicion_id]  = (double) round($preguntaMDato->porc, $decimales);
        }

        
        
        
        $sqlDimension = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from (";
        $sqlDimension .= " $sqlPregunta ";
        $sqlDimension .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion";
        // dd($sqlDimension);
        
        $dimensionDatos = DB::select($sqlDimension);
        foreach ($dimensionDatos as $key => $dimensionDato){
            $arrarSalida['S_'.$dimensionDato->cod_seccion]['sub_seccion']['ss_'.$dimensionDato->sub_seccion_id]['dimension']['d_'.$dimensionDato->dimension_id]['dato']['dimension_descripcion'] = $dimensionDato->dimension_descripcion;
            $arrarSalida['S_'.$dimensionDato->cod_seccion]['sub_seccion']['ss_'.$dimensionDato->sub_seccion_id]['dimension']['d_'.$dimensionDato->dimension_id]['dato']['porc'] = $dimensionDato->porc;
        }
        
        
        
        // -- Evolutivo

        $sqlDimensionM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, medicion_id, avg(porc) as porc from (";
        $sqlDimensionM .= " $sqlPreguntaM ";
        $sqlDimensionM .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion, medicion_id";

        // dd($sqlDimensionM);

        $dimensionMDatos = DB::select($sqlDimensionM);
        foreach ($dimensionMDatos as $key => $dimensionMDato){
            $arrarSalida['S_'.$dimensionMDato->cod_seccion]['sub_seccion']['ss_'.$dimensionMDato->sub_seccion_id]['dimension']['d_'.$dimensionMDato->dimension_id]['medicion'][$dimensionMDato->medicion_id] = (double) round($dimensionMDato->porc, $decimales);
        }

        // --SubSeccion

        $sqlSubSecccion = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from ( ";
        $sqlSubSecccion .= " $sqlDimension ";
        $sqlSubSecccion .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion";

        // dd($sqlSubSecccion);


        $subSeccionDatos = DB::select($sqlSubSecccion);
        foreach ($subSeccionDatos as $key => $subSeccionDato){
            $arrarSalida['S_'.$subSeccionDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionDato->sub_seccion_id]['dato']['titulo_sub_seccion'] = $subSeccionDato->literal_sub_seccion;
            $arrarSalida['S_'.$subSeccionDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionDato->sub_seccion_id]['dato']['porc'] = $subSeccionDato->porc;
        }

        // -- Evolutivo 

        $sqlSubSecccionM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, medicion_id, avg(porc) as porc from ( ";
        $sqlSubSecccionM .= " $sqlDimensionM ";
        $sqlSubSecccionM .= ") as dato3 group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, medicion_id ";


        $subSeccionMDatos = DB::select($sqlSubSecccionM);
        foreach ($subSeccionMDatos as $key => $subSeccionMDato){
            $arrarSalida['S_'.$subSeccionMDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionMDato->sub_seccion_id]['medicion'][$subSeccionMDato->medicion_id][] = round($subSeccionMDato->porc, $decimales);
        }

        // dd($arrarSalida);
        
        // -- Seccion
        
        $sqlSecccion = "select cod_seccion, titulo_seccion, avg(porc) as porc from (";
        $sqlSecccion .= " $sqlSubSecccion ";
        $sqlSecccion .= ") as datos group by cod_seccion, titulo_seccion";
        
        
        
        $seccionDatos = DB::select($sqlSecccion);
        foreach ($seccionDatos as $key => $seccionDato){
            $arrarSalida['S_'.$seccionDato->cod_seccion]['dato']['titulo_seccion'] = $seccionDato->titulo_seccion;
            $arrarSalida['S_'.$seccionDato->cod_seccion]['dato']['porc'] = $seccionDato->porc;
            $grafico[] = (double) round($seccionDato->porc, $decimales);
        }

        
        // -- Evolutivo 
        
        $sqlSecccionM = "select cod_seccion, titulo_seccion, medicion_id, avg(porc) as porc from (";
        $sqlSecccionM .= " $sqlSubSecccionM ";
        $sqlSecccionM .= ") as datos group by cod_seccion, titulo_seccion, medicion_id";

        // dd($sqlSecccionM);
        
        
        $seccionMDatos = DB::select($sqlSecccionM);
        foreach ($seccionMDatos as $key => $seccionMDato){
            $arrarSalida['S_'.$seccionMDato->cod_seccion]['medicion'][$seccionMDato->medicion_id] = (int) round($seccionMDato->porc, $decimales);
        }
        
        // dd($arrarSalida);

        // Total

        $sqlTotal = "select avg(porc)  as porc from (";
        $sqlTotal .= " $sqlSecccion ";
        $sqlTotal .= ") as datos";

        $totalDatos = DB::select($sqlTotal);
        foreach ($totalDatos as $key => $totalDato){
            // $puntajeTotal = (int) round($totalDato->porc, $decimales);
            $puntajeTotal = round((double) $totalDato->porc, $decimales);
        }
        

        // $puntajeTotal = 85;

        return $this->sendResponse(['dato' => $arrarSalida, 'grafico' => $grafico, 'total' => $puntajeTotal], 'Puntos de cotacto');


    }

    public function getListByMedicion(Request $request){

        // dd($request->all());

        $where = $this->getWhere($request, ['seccion_id' => true, 'medicion_id' => true]);

        // dd($where);

        $decimales = 2;
        
        $sql = "select datos.*, tareas.fecha_completacion from tareas, ( ";
        $sql .= "select cod_tarea, cod_sucursal, nombre_sucursal, round(avg(porc), $decimales) as porc from (  ";
        $sql .= "select cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, avg(porc) as porc from (  ";
        $sql .= "select cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from (   ";
        $sql .= "select cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from (  ";
        $sql .= "select cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta,  sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from (  ";
        
        $sql .= "select datos_consolidados.cod_tarea, sucursals.cod_sucursal, sucursals.nombre_sucursal, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,   ";
        $sql .= "dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id, medicions.descripcion_corto,  count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc   ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions    ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND    ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and    ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and    ";
        $sql .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno                                      $where ";
        $sql .= "group by datos_consolidados.cod_tarea, cod_sucursal, nombre_sucursal, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,   ";
        $sql .= "dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id, medicions.descripcion_corto    ";
        $sql .= "order by cod_seccion, dimension_id  ";
        
        $sql .= ") as datos group by cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta  ";
        $sql .= ") as datos group by cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion  ";
        $sql .= ") as datos group by cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion  ";
        $sql .= ") as datos group by cod_tarea, cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion  ";
        $sql .= ") as datos group by cod_tarea, cod_sucursal, nombre_sucursal order by cod_sucursal asc  ";
        $sql .= ") as datos where tareas.cod_sucursal = datos.cod_sucursal and tareas.cod_tarea = datos.cod_tarea ";

        

        // dd($sql);

        $datos = DB::select($sql);

        $etiquetas = [];
        $valores = [];
        
        $colores = [];
        $bordes = [];

        foreach ($datos as $key => $dato){
            $valores[] = round($dato->porc, 0);
            $etiquetas[] = $dato->nombre_sucursal;
            
            $color = '';
            $borde = '';
            
            if (str_contains($dato->nombre_sucursal, 'Lider')){
                $color = 'rgba(3, 101, 192, 0.5)';
                $borde = 'rgb(3, 101, 192)';
            }
            else if (str_contains($dato->nombre_sucursal, 'Express')){
                $color = 'rgba(0, 176, 240, 0.5)';
                $borde = 'rgb(0, 176, 240)';
            }
            else if (str_contains($dato->nombre_sucursal, 'SBA')){
                $color = 'rgba(200, 37, 6, 0.5)';
                $borde = 'rgb(200, 37, 6)';
            }

            $colores[] = $color;
            $bordes[] = $borde;
        }

        return $this->sendResponse(
            [
                'dato' => $datos,
                'valores' => $valores,
                'etiquetas' => $etiquetas,
                'colores' => $colores,
                'bordes' => $bordes
            ]
        , 'Ranking local');
}

    
}
