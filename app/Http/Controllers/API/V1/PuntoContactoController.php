<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PuntoContactoController extends BaseController
{
    public function index($filtros){
        return "filtros";
    }

    public function getFilterGraficoTotales(Request $request){
        // Incluye filtro medicion

        $sqlWhere = "";

        $decimales = 2;


        if ($request['filtro']['medicion_id'] != 0){
            // $where .= " and medicions.id = $medicion_id ";
            $sqlWhere .= " and medicions.id = " . $request['filtro']['medicion_id'] . " ";
        }

        if ($request['filtro']['formato_id'] != 0){
            $sqlWhere .= "and formatos_sucursals.formato_id = " . $request['filtro']['formato_id'] . " ";
        }
        else {
            $sqlWhere .= "and formatos_sucursals.formato_id = 1 ";
        }

        if ($request['filtro']['sucursal_id'] != 0){
            $sqlWhere .= "and sucursals.cod_sucursal = " . $request['filtro']['sucursal_id'] . " ";
        }
        if ($request['filtro']['seccion_id'] != 0){
            $sqlWhere .= "and seccions.cod_seccion = " . $request['filtro']['seccion_id'] . " ";
        }

        $arrarSalida = [];
        $grafico = [];
        $puntajeTotal = 0;


        // $sqlWhere = "and formatos.id = 3 and medicions.id = 1";

        $sqlBase = "select seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id,  ";
        $sqlBase .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc ";
        $sqlBase .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions  ";
        $sqlBase .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sqlBase .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sqlBase .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sqlBase .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno              $sqlWhere               ";
        $sqlBase .= "group by seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id  ";
        $sqlBase .= "order by cod_seccion, dimension_id";

        


        $sqlPregunta = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from (";
        $sqlPregunta .= " $sqlBase ";
        $sqlPregunta .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta";


        $preguntaDatos = DB::select($sqlPregunta);
        foreach ($preguntaDatos as $key => $preguntaDato){
            $arrarSalida['S_'.$preguntaDato->cod_seccion]['sub_seccion']['ss_'.$preguntaDato->sub_seccion_id]['dimension']['d_'.$preguntaDato->dimension_id]['preguntas']['p_'.$preguntaDato->cod_interno]['dato']  = $preguntaDato;
        }

        // ['sub_seccion']['ss_'.$preguntaDato->sub_seccion_id]
        // -- Evolutivo
        
        $sqlPreguntaM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, medicion_id, avg(porc) as porc from (";
        $sqlPreguntaM .= " $sqlBase ";
        $sqlPreguntaM .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, medicion_id";
        
        
        $preguntaMDatos = DB::select($sqlPreguntaM);
        foreach ($preguntaMDatos as $key => $preguntaMDato){
            $arrarSalida['S_'.$preguntaMDato->cod_seccion]['sub_seccion']['ss_'.$preguntaMDato->sub_seccion_id]['dimension']['d_'.$preguntaMDato->dimension_id]['preguntas']['p_'.$preguntaMDato->cod_interno]['medicion'][$preguntaMDato->medicion_id]  = (double) round($preguntaMDato->porc, $decimales);
        }

        
        
        
        $sqlDimension = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from (";
        $sqlDimension .= " $sqlPregunta ";
        $sqlDimension .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion";
        // dd($sqlDimension);
        
        $dimensionDatos = DB::select($sqlDimension);
        foreach ($dimensionDatos as $key => $dimensionDato){
            $arrarSalida['S_'.$dimensionDato->cod_seccion]['sub_seccion']['ss_'.$dimensionDato->sub_seccion_id]['dimension']['d_'.$dimensionDato->dimension_id]['dato']['dimension_descripcion'] = $dimensionDato->dimension_descripcion;
            $arrarSalida['S_'.$dimensionDato->cod_seccion]['sub_seccion']['ss_'.$dimensionDato->sub_seccion_id]['dimension']['d_'.$dimensionDato->dimension_id]['dato']['porc'] = round($dimensionDato->porc, $decimales);
        }
        
        
        
        // -- Evolutivo

        $sqlDimensionM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, medicion_id, avg(porc) as porc from (";
        $sqlDimensionM .= " $sqlPreguntaM ";
        $sqlDimensionM .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion, medicion_id";

        // dd($sqlDimensionM);

        $dimensionMDatos = DB::select($sqlDimensionM);
        foreach ($dimensionMDatos as $key => $dimensionMDato){
            $arrarSalida['S_'.$dimensionMDato->cod_seccion]['sub_seccion']['ss_'.$dimensionMDato->sub_seccion_id]['dimension']['d_'.$dimensionMDato->dimension_id]['medicion'][$dimensionMDato->medicion_id] = (double) round($dimensionMDato->porc, $decimales);
        }

        // --SubSeccion

        $sqlSubSecccion = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from ( ";
        $sqlSubSecccion .= " $sqlDimension ";
        $sqlSubSecccion .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion";

        // dd($sqlSubSecccion);


        $subSeccionDatos = DB::select($sqlSubSecccion);
        foreach ($subSeccionDatos as $key => $subSeccionDato){
            $arrarSalida['S_'.$subSeccionDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionDato->sub_seccion_id]['dato']['titulo_sub_seccion'] = $subSeccionDato->literal_sub_seccion;
            $arrarSalida['S_'.$subSeccionDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionDato->sub_seccion_id]['dato']['porc'] = round($subSeccionDato->porc, $decimales);
        }

        // -- Evolutivo 

        $sqlSubSecccionM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, medicion_id, avg(porc) as porc from ( ";
        $sqlSubSecccionM .= " $sqlDimensionM ";
        $sqlSubSecccionM .= ") as dato3 group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, medicion_id ";


        $subSeccionMDatos = DB::select($sqlSubSecccionM);
        foreach ($subSeccionMDatos as $key => $subSeccionMDato){
            $arrarSalida['S_'.$subSeccionMDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionMDato->sub_seccion_id]['medicion'][$subSeccionMDato->medicion_id][] = round($subSeccionMDato->porc, $decimales);
        }

        // dd($arrarSalida);
        
        // -- Seccion
        
        $sqlSecccion = "select cod_seccion, titulo_seccion, avg(porc) as porc from (";
        $sqlSecccion .= " $sqlSubSecccion ";
        $sqlSecccion .= ") as datos group by cod_seccion, titulo_seccion";
        
        
        
        $seccionDatos = DB::select($sqlSecccion);
        foreach ($seccionDatos as $key => $seccionDato){
            $arrarSalida['S_'.$seccionDato->cod_seccion]['dato']['titulo_seccion'] = $seccionDato->titulo_seccion;
            $arrarSalida['S_'.$seccionDato->cod_seccion]['dato']['porc'] = round($seccionDato->porc, $decimales);
            $grafico[] = (double) round($seccionDato->porc, $decimales);
        }

        
        // -- Evolutivo 
        
        $sqlSecccionM = "select cod_seccion, titulo_seccion, medicion_id, avg(porc) as porc from (";
        $sqlSecccionM .= " $sqlSubSecccionM ";
        $sqlSecccionM .= ") as datos group by cod_seccion, titulo_seccion, medicion_id";

        // dd($sqlSecccionM);
        
        
        $seccionMDatos = DB::select($sqlSecccionM);
        foreach ($seccionMDatos as $key => $seccionMDato){
            $arrarSalida['S_'.$seccionMDato->cod_seccion]['medicion'][$seccionMDato->medicion_id] = (int) round($seccionMDato->porc, $decimales);
        }
        
        // dd($arrarSalida);

        // Total

        $sqlTotal = "select avg(porc)  as porc from (";
        $sqlTotal .= " $sqlSecccion ";
        $sqlTotal .= ") as datos";

        $totalDatos = DB::select($sqlTotal);
        foreach ($totalDatos as $key => $totalDato){
            // $puntajeTotal = (int) round($totalDato->porc, $decimales);
            $puntajeTotal = round((double) $totalDato->porc, $decimales);
        }
        

        // $puntajeTotal = 85;

        return $this->sendResponse(['grafico' => $grafico, 'total' => $puntajeTotal], 'Puntos de cotacto');


    }

    public function getFilter2(Request $request){
        // Sin filtro de medicion

        $sqlWhere = "";

        $decimales = 2;


        // if ($request['filtro']['medicion_id'] != 0){
        //     // $where .= " and medicions.id = $medicion_id ";
        //     $sqlWhere .= " and medicions.id = " . $request['filtro']['medicion_id'] . " ";
        // }

        if ($request['filtro']['formato_id'] != 0){
            $sqlWhere .= "and formatos_sucursals.formato_id = " . $request['filtro']['formato_id'] . " ";
        }
        else {
            $sqlWhere .= "and formatos_sucursals.formato_id = 1 ";
        }

        if ($request['filtro']['sucursal_id'] != 0){
            $sqlWhere .= "and sucursals.cod_sucursal = " . $request['filtro']['sucursal_id'] . " ";
        }
        if ($request['filtro']['seccion_id'] != 0){
            $sqlWhere .= "and seccions.cod_seccion = " . $request['filtro']['seccion_id'] . " ";
        }

        $arrarSalida = [];
        $grafico = [];
        $puntajeTotal = 0;


        // $sqlWhere = "and formatos.id = 3 and medicions.id = 1";

        $sqlBase = "select seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id,  ";
        $sqlBase .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc ";
        $sqlBase .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions  ";
        $sqlBase .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sqlBase .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sqlBase .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sqlBase .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno              $sqlWhere               ";
        $sqlBase .= "group by seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion, dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id  ";
        $sqlBase .= "order by cod_seccion, dimension_id";

        


        $sqlPregunta = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from (";
        $sqlPregunta .= " $sqlBase ";
        $sqlPregunta .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta";


        $preguntaDatos = DB::select($sqlPregunta);
        foreach ($preguntaDatos as $key => $preguntaDato){
            $arrarSalida['S_'.$preguntaDato->cod_seccion]['sub_seccion']['ss_'.$preguntaDato->sub_seccion_id]['dimension']['d_'.$preguntaDato->dimension_id]['preguntas']['p_'.$preguntaDato->cod_interno]['dato']  = $preguntaDato;
        }

        // ['sub_seccion']['ss_'.$preguntaDato->sub_seccion_id]
        // -- Evolutivo
        
        $sqlPreguntaM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, medicion_id, avg(porc) as porc from (";
        $sqlPreguntaM .= " $sqlBase ";
        $sqlPreguntaM .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta, medicion_id";
        
        
        $preguntaMDatos = DB::select($sqlPreguntaM);
        foreach ($preguntaMDatos as $key => $preguntaMDato){
            $arrarSalida['S_'.$preguntaMDato->cod_seccion]['sub_seccion']['ss_'.$preguntaMDato->sub_seccion_id]['dimension']['d_'.$preguntaMDato->dimension_id]['preguntas']['p_'.$preguntaMDato->cod_interno]['medicion'][$preguntaMDato->medicion_id]  = (double) round($preguntaMDato->porc, $decimales);
        }

        
        
        
        $sqlDimension = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from (";
        $sqlDimension .= " $sqlPregunta ";
        $sqlDimension .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion";
        // dd($sqlDimension);
        
        $dimensionDatos = DB::select($sqlDimension);
        foreach ($dimensionDatos as $key => $dimensionDato){
            $arrarSalida['S_'.$dimensionDato->cod_seccion]['sub_seccion']['ss_'.$dimensionDato->sub_seccion_id]['dimension']['d_'.$dimensionDato->dimension_id]['dato']['dimension_descripcion'] = $dimensionDato->dimension_descripcion;
            $arrarSalida['S_'.$dimensionDato->cod_seccion]['sub_seccion']['ss_'.$dimensionDato->sub_seccion_id]['dimension']['d_'.$dimensionDato->dimension_id]['dato']['porc'] = round($dimensionDato->porc, $decimales);
        }
        
        
        
        // -- Evolutivo

        $sqlDimensionM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, medicion_id, avg(porc) as porc from (";
        $sqlDimensionM .= " $sqlPreguntaM ";
        $sqlDimensionM .= ") as datos group by cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion, medicion_id";

        // dd($sqlDimensionM);

        $dimensionMDatos = DB::select($sqlDimensionM);
        foreach ($dimensionMDatos as $key => $dimensionMDato){
            $arrarSalida['S_'.$dimensionMDato->cod_seccion]['sub_seccion']['ss_'.$dimensionMDato->sub_seccion_id]['dimension']['d_'.$dimensionMDato->dimension_id]['medicion'][$dimensionMDato->medicion_id] = (double) round($dimensionMDato->porc, $decimales);
        }

        // --SubSeccion

        $sqlSubSecccion = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from ( ";
        $sqlSubSecccion .= " $sqlDimension ";
        $sqlSubSecccion .= ") as datos group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion";

        // dd($sqlSubSecccion);


        $subSeccionDatos = DB::select($sqlSubSecccion);
        foreach ($subSeccionDatos as $key => $subSeccionDato){
            $arrarSalida['S_'.$subSeccionDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionDato->sub_seccion_id]['dato']['titulo_sub_seccion'] = $subSeccionDato->literal_sub_seccion;
            $arrarSalida['S_'.$subSeccionDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionDato->sub_seccion_id]['dato']['porc'] = round($subSeccionDato->porc, $decimales);
        }

        // -- Evolutivo 

        $sqlSubSecccionM = "select cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, medicion_id, avg(porc) as porc from ( ";
        $sqlSubSecccionM .= " $sqlDimensionM ";
        $sqlSubSecccionM .= ") as dato3 group by cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, medicion_id ";


        $subSeccionMDatos = DB::select($sqlSubSecccionM);
        foreach ($subSeccionMDatos as $key => $subSeccionMDato){
            $arrarSalida['S_'.$subSeccionMDato->cod_seccion]['sub_seccion']['ss_'.$subSeccionMDato->sub_seccion_id]['medicion'][$subSeccionMDato->medicion_id][] = round($subSeccionMDato->porc, $decimales);
        }

        // dd($arrarSalida);
        
        // -- Seccion
        
        $sqlSecccion = "select cod_seccion, titulo_seccion, avg(porc) as porc from (";
        $sqlSecccion .= " $sqlSubSecccion ";
        $sqlSecccion .= ") as datos group by cod_seccion, titulo_seccion";
        
        
        
        $seccionDatos = DB::select($sqlSecccion);
        foreach ($seccionDatos as $key => $seccionDato){
            $arrarSalida['S_'.$seccionDato->cod_seccion]['dato']['titulo_seccion'] = $seccionDato->titulo_seccion;
            $arrarSalida['S_'.$seccionDato->cod_seccion]['dato']['porc'] = round($seccionDato->porc, $decimales);
            $grafico[] = (double) round($seccionDato->porc, $decimales);
        }

        
        // -- Evolutivo 
        
        $sqlSecccionM = "select cod_seccion, titulo_seccion, medicion_id, avg(porc) as porc from (";
        $sqlSecccionM .= " $sqlSubSecccionM ";
        $sqlSecccionM .= ") as datos group by cod_seccion, titulo_seccion, medicion_id";

        // dd($sqlSecccionM);
        
        
        $seccionMDatos = DB::select($sqlSecccionM);
        foreach ($seccionMDatos as $key => $seccionMDato){
            $arrarSalida['S_'.$seccionMDato->cod_seccion]['medicion'][$seccionMDato->medicion_id] = (double) round($seccionMDato->porc, $decimales);
        }
        
        // dd($arrarSalida);

        // Total

        $sqlTotal = "select avg(porc)  as porc from (";
        $sqlTotal .= " $sqlSecccion ";
        $sqlTotal .= ") as datos";

        $totalDatos = DB::select($sqlTotal);
        foreach ($totalDatos as $key => $totalDato){
            // $puntajeTotal = (int) round($totalDato->porc, $decimales);
            $puntajeTotal = round((double) $totalDato->porc, $decimales);
        }
        

        // $puntajeTotal = 85;

        return $this->sendResponse(['dato' => $arrarSalida, 'grafico' => $grafico, 'total' => $puntajeTotal], 'Puntos de cotacto');


    }




    

    public function getFilter(Request $request){
        // dd($request['filtro']);

        $sqlWhere = "";


        if ($request['filtro']['medicion_id'] != 0){
            // $where .= " and medicions.id = $medicion_id ";
            $sqlWhere .= " and medicions.id = " . $request['filtro']['medicion_id'] . " ";
        }

        if ($request['filtro']['formato_id'] != 0){
            $sqlWhere .= "and formatos_sucursals.formato_id = " . $request['filtro']['formato_id'] . " ";
        }
        if ($request['filtro']['sucursal_id'] != 0){
            $sqlWhere .= "and sucursals.cod_sucursal = " . $request['filtro']['sucursal_id'] . " ";
        }
        if ($request['filtro']['seccion_id'] != 0){
            $sqlWhere .= "and seccions.cod_seccion = " . $request['filtro']['seccion_id'] . " ";
        }

        $arrarSalida = [];

        $sqlTotal = "select medicions.id as medicion_id, (sum(datos_consolidados.puntaje) / count(*)) * 100 as porc ";
        $sqlTotal .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sqlTotal .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sqlTotal .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sqlTotal .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sqlTotal .= "medicion_tarea.medicion_id = medicions.id " . $sqlWhere;
        $sqlTotal .= "group by medicions.id ";

        $puntoContactoTotal = DB::select($sqlTotal);
        //dd($puntoContactoTotal);

        // foreach ($puntoContactoTotal as $key => $puntoTotal){
        //     $arrarSalida['total']['m'.$puntoTotal->medicion_id] = round($puntoTotal->porc, 0);
        // }

        // Filtros
        $sql = "select dimensions.id, dimensions.descripcion as dimension_txt, count(*) as count, sum(datos_consolidados.puntaje), (sum(datos_consolidados.puntaje) / count(*)) * 100 as porc ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sql .= "medicion_tarea.medicion_id = medicions.id " . $sqlWhere;
        $sql .= "group by dimensions.id, dimensions.descripcion ";
        $sql .= "order by dimensions.id ";

        $punto_contactos = DB::select($sql);

        
        
        foreach ($punto_contactos as $key => $punto){
            // dd($key);

            $arrarSalida [$key]["id"] = $punto->id;
            $arrarSalida [$key]["dimension_txt"] = $punto->dimension_txt;
            $arrarSalida [$key]["porc"] = round($punto->porc, 0);

            $sqlPunto = "select dimensions.id as dimension_id, medicions.id as medicion_id, count(*) as count, sum(datos_consolidados.puntaje), (sum(datos_consolidados.puntaje) / count(*)) * 100 as porc ";
            $sqlPunto .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions ";
            $sqlPunto .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
            $sqlPunto .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
            $sqlPunto .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
            $sqlPunto .= "medicion_tarea.medicion_id = medicions.id and dimensions.id = " . $punto->id . " " . $sqlWhere;
            $sqlPunto .= "group by dimensions.id, medicions.id ";
            $sqlPunto .= "order by dimensions.id desc;";
            // dd($sqlPunto);

            $puntoMeses = DB::select($sqlPunto);

            foreach ($puntoMeses as $puntoMes){
                // $puntoMesAttay = (array)$puntoMes;
                // $punto_contactos[$key]['evolutivo']['dimension_id'] = 0;
                $arrarSalida [$key]['m'.$puntoMes->medicion_id] = round($puntoMes->porc, 0);
            }

            $sqlPuntoPregunta = "select preguntas.cod_interno, preguntas.literal_pregunta, count(*) as count, sum(datos_consolidados.puntaje), (sum(datos_consolidados.puntaje) / count(*)) * 100 as porc ";
            $sqlPuntoPregunta .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions ";
            $sqlPuntoPregunta .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
            $sqlPuntoPregunta .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
            $sqlPuntoPregunta .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
            $sqlPuntoPregunta .= "medicion_tarea.medicion_id = medicions.id and dimensions.id = " . $punto->id . " " . $sqlWhere;
            $sqlPuntoPregunta .= "group by preguntas.cod_interno, preguntas.literal_pregunta ";
            $sqlPuntoPregunta .= "order by preguntas.cod_interno;";

            // dd($sqlPuntoPregunta);

            $puntoMesesPregunta = DB::select($sqlPuntoPregunta);

            $i = 0;
            foreach ($puntoMesesPregunta as $puntoMesPregunta){
                $arrarSalida [$key]['preguntas'][$i]['cod_interno'] = $puntoMesPregunta->cod_interno;
                $arrarSalida [$key]['preguntas'][$i]['literal'] = $puntoMesPregunta->literal_pregunta;
                $arrarSalida [$key]['preguntas'][$i]['porc'] = round($puntoMesPregunta->porc, 0);

                $sqlPuntoPreguntaMes = "select dimensions.id as dimension_id, medicions.id as medicion_id, count(*) as count, sum(datos_consolidados.puntaje), (sum(datos_consolidados.puntaje) / count(*)) * 100 as porc ";
                $sqlPuntoPreguntaMes .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions ";
                $sqlPuntoPreguntaMes .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
                $sqlPuntoPreguntaMes .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
                $sqlPuntoPreguntaMes .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
                $sqlPuntoPreguntaMes .= "medicion_tarea.medicion_id = medicions.id and dimensions.id = " . $punto->id . " and preguntas.cod_interno = '" . $puntoMesPregunta->cod_interno . "' " . $sqlWhere;
                $sqlPuntoPreguntaMes .= "group by dimensions.id, medicions.id ";
                $sqlPuntoPreguntaMes .= "order by dimensions.id desc;";

                //dd($sqlPuntoPreguntaMes);

                $puntoMesesPreguntaMeses = DB::select($sqlPuntoPreguntaMes);

                foreach ($puntoMesesPreguntaMeses as $puntoMesesPreguntaMes){
                    // dd($puntoMesesPreguntaMes);
                    // $arrarSalida [$key]['preguntas'][$i]['cod_interno'] = $puntoMesPregunta->cod_interno;
                    // $arrarSalida [$key]['preguntas'][$i]['literal'] = $puntoMesPregunta->literal_pregunta;
                    // $arrarSalida [$key]['preguntas'][$i]['porc'] = round($puntoMesPregunta->porc, 0);
                    $arrarSalida [$key]['preguntas'][$i]['m'.$puntoMesesPreguntaMes->medicion_id] = round($puntoMesesPreguntaMes->porc, 0);
                }


                $i++;
            }

            
        }
        
        // dd($arrarSalida);
        // dd($punto_contactos);

        return $this->sendResponse($arrarSalida, 'Puntos de cotacto');

    }
}
