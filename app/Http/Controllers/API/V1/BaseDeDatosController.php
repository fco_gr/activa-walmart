<?php

namespace App\Http\Controllers\API\V1;

use App\Dato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseDeDatosController extends Controller
{
    public function DescargaBase(Request $request){
        $db = Dato::all();
        return $db;
    }
}
