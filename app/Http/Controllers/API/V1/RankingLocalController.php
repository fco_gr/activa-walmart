<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RankingLocalController extends BaseController
{
    public function getRanking(Request $request){

        // dd($request->all());

        $medicion_id = $request->filtro['medicion_id'];
        $codSucursal = $request->filtro['sucursal_id'];
        $formato_id = $request->filtro['formato_id'];
        $dimension_id = $request->filtro['dimension_id'];
        $seccion_id = $request->filtro['seccion_id'];
        
        $where = '';

        if ($medicion_id){
            // $where .= " and medicions.id = $medicion_id ";
            $where .= " and medicions.id = " . $medicion_id . " ";
        }

        if($codSucursal){
            $where .= " and sucursals.cod_sucursal = $codSucursal ";
        }
        
        if($formato_id){
            $where .= " and formatos.id = $formato_id ";
        }
        // else {
        //     $where .= " and formatos.id = 1";
        // }
        
        if($dimension_id){
            $where .= " and dimensions.id = $dimension_id ";
        }

        if($seccion_id){
            $where .= " and seccions.cod_seccion = $seccion_id ";
        }

        $decimales = 2;
        
        $sql = "select cod_sucursal, nombre_sucursal, round(avg(porc), $decimales) as porc from ( ";
        $sql .= "select cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, avg(porc) as porc from ( ";
        $sql .= "select cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, avg(porc) porc from (  ";
        $sql .= "select cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from ( ";
        $sql .= "select cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta,  ";
        $sql .= "sum(suma) as suma, sum(cuenta) as cuenta, sum(suma)/sum(cuenta) * 100 as porc from ( ";
        $sql .= "select sucursals.cod_sucursal, sucursals.nombre_sucursal, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,  ";
        $sql .= "dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id as medicion_id, medicions.descripcion_corto,  ";
        $sql .= "count(*) as cuenta, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc  ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions, pregunta_sub_seccions   ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND   ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and   ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and   ";
        $sql .= "medicion_tarea.medicion_id = medicions.id and preguntas.cod_interno = pregunta_sub_seccions.cod_interno                                  $where    ";
        $sql .= "group by cod_sucursal, nombre_sucursal, seccions.cod_seccion, seccions.titulo_seccion, sub_seccion_id, literal_sub_seccion,  ";
        $sql .= "dimensions.id, dimensions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta, medicions.id, medicions.descripcion_corto   ";
        $sql .= "order by cod_seccion, dimension_id ";
        $sql .= ") as datos group by cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion, dimension_id, dimension_descripcion, cod_interno, literal_pregunta ";
        $sql .= ") as datos group by cod_sucursal, nombre_sucursal, cod_seccion, sub_seccion_id, literal_sub_seccion, titulo_seccion, dimension_id, dimension_descripcion ";
        $sql .= ") as datos group by cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion, sub_seccion_id, literal_sub_seccion ";
        $sql .= ") as datos group by cod_sucursal, nombre_sucursal, cod_seccion, titulo_seccion ";
        $sql .= ") as datos group by cod_sucursal, nombre_sucursal order by porc desc";

        // dd($sql);

        $datos = DB::select($sql);

        $etiquetas = [];
        $valores = [];
        
        $colores = [];
        $bordes = [];

        foreach ($datos as $key => $dato){
            $valores[] = round($dato->porc, $decimales);
            $etiquetas[] = $dato->nombre_sucursal;
            
            $color = '';
            $borde = '';
            
            if (str_contains($dato->nombre_sucursal, 'Lider')){
                $color = 'rgba(3, 101, 192, 0.5)';
                $borde = 'rgb(3, 101, 192)';
            }
            else if (str_contains($dato->nombre_sucursal, 'Express')){
                $color = 'rgba(0, 176, 240, 0.5)';
                $borde = 'rgb(0, 176, 240)';
            }
            else if (str_contains($dato->nombre_sucursal, 'SBA')){
                $color = 'rgba(200, 37, 6, 0.5)';
                $borde = 'rgb(200, 37, 6)';
            }

            $colores[] = $color;
            $bordes[] = $borde;
        }

        return $this->sendResponse(
            [
                'dato' => $datos,
                'valores' => $valores,
                'etiquetas' => $etiquetas,
                'colores' => $colores,
                'bordes' => $bordes
            ]
        , 'Ranking local');
    }
}
