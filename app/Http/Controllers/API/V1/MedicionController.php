<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Medicion;
use Illuminate\Http\Request;

class MedicionController extends BaseController
{
    protected $medicion = '';

    public function __construct(Medicion $medicion)
    {
        $this->middleware('auth');
        $this->medicion = $medicion;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medicions = Medicion::orderBy('fecha_numero', 'DESC')->get();
        return $this->sendResponse($medicions, 'Listado Mediciones');
    }

    public function list()
    {
        $medicions = $this->medicion->orderBy('fecha_numero', 'DESC')->pluck('descripcion', 'id');
        return $this->sendResponse($medicions, 'Listado Mediciones');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medicion  $medicion
     * @return \Illuminate\Http\Response
     */
    public function show(Medicion $medicion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medicion  $medicion
     * @return \Illuminate\Http\Response
     */
    public function edit(Medicion $medicion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medicion  $medicion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medicion $medicion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medicion  $medicion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medicion $medicion)
    {
        //
    }
}
