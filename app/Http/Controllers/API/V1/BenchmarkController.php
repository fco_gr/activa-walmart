<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BenchmarkController extends BaseController
{

    public function index(Request $request)
    {
        // dd($request->all());

        $medicion_id = $request->filtro['medicion_id'];
        $codSucursal = $request->filtro['sucursal_id'];
        $formato_id = $request->filtro['formato_id'];
        $dimension_id = $request->filtro['dimension_id'];
        $seccion_id = $request->filtro['seccion_id'];
        
        $where = '';

        if($medicion_id){
            $where .= " and medicions.id = $medicion_id ";
        }

        if($codSucursal){
            $where .= " and sucursals.cod_sucursal = $codSucursal ";
        }
        
        if($formato_id){
            $where .= " and formatos.id = $formato_id ";
        } else {
            $where .= " and formatos.id = 1";
        }

        if($dimension_id){
            $where .= " and dimensions.id = $dimension_id ";
        }

        if($seccion_id){
            $where .= " and seccions.cod_seccion = $seccion_id ";
        }

        $resultadoPorFormato = [];

        $sql = "select cod_seccion, titulo_seccion, dimension_id, dimension_descripcion, avg(porc) as porc from ( ";
        $sql .= "select cod_seccion, titulo_seccion, dimension_id, dimension_descripcion, cod_sucursal, nombre_sucursal, avg(porc) as porc from ( ";
        $sql .= "select seccions.cod_seccion, seccions.titulo_seccion, dimensions.id as dimension_id, dimensions.descripcion as dimension_descripcion, sucursals.cod_sucursal, sucursals.nombre_sucursal,  ";
        $sql .= "medicions.id as medicion_id, medicions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta,  ";
        $sql .= "count(*) as count, sum(datos_consolidados.puntaje) as suma, (sum(datos_consolidados.puntaje) / count(*) * 100)  as porc ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sql .= "medicion_tarea.medicion_id = medicions.id       $where  ";
        $sql .= "group by seccions.cod_seccion, seccions.titulo_seccion, dimensions.id, dimensions.descripcion, sucursals.cod_sucursal, sucursals.nombre_sucursal,  ";
        $sql .= "medicions.id, medicions.descripcion, preguntas.cod_interno, preguntas.literal_pregunta   ";
        $sql .= "order by cod_seccion, dimension_id ";
        $sql .= ") as datos group by cod_seccion, titulo_seccion, dimension_id, dimension_descripcion, cod_sucursal, nombre_sucursal ";
        $sql .= ") as datos2 group by cod_seccion, titulo_seccion, dimension_id, dimension_descripcion";
        
        // dd($sql);
        

        $datos = DB::select($sql);
        foreach ($datos as $dato){
            $resultadoPorFormato['D'.$dato->dimension_id][] = (int) round($dato->porc, 0);
        }


        $sqlTotal = "select dimension_id, dimension_descripcion, avg(porc) as porc from ("; 
        $sqlTotal .= " $sql ";
        $sqlTotal .= ") as dato3 group by dimension_id, dimension_descripcion";

        $datosTotales = DB::select($sqlTotal);
        foreach ($datosTotales as $datoT){
            array_unshift($resultadoPorFormato['D'.$datoT->dimension_id], (int) round($datoT->porc, 0));
        }

        $sqlSeccion = "select cod_seccion, titulo_seccion, avg(porc) as porc from ( ";
        $sqlSeccion .= " $sql ";
        $sqlSeccion .= ") as dato3 group by cod_seccion, titulo_seccion";

        // dd($sqlSeccion);

        $datosSeccions = DB::select($sqlSeccion);
        foreach ($datosSeccions as $datoS){
            // array_unshift($resultadoPorFormato['D'.$datoT->dimension_id], (int) round($datoS->porc, 0));
            $resultadoPorFormato['D9'][] = (int) round($datoS->porc, 0);
        }

        $sqlSeccionT = "select avg(porc) as porc from ( ";
        $sqlSeccionT .= " $sql ";
        $sqlSeccionT .= ") as dato4";

        $datosTSeccions = DB::select($sqlSeccionT);
        foreach ($datosTSeccions as $datoST){
            // dd($datoST);
            array_unshift($resultadoPorFormato['D9'], (int) round($datoST->porc, 0));
            // $resultadoPorFormato['D9'][] = (int) round($dato->porc, 0);
        }


        return $this->sendResponse($resultadoPorFormato, 'Listado sección y dimensiones');

    }


    public function index2(Request $request)
    {
        // dd($request->all());

        $medicion_id = $request->filtro['medicion_id'];
        $codSucursal = $request->filtro['sucursal_id'];
        $formato_id = $request->filtro['formato_id'];
        $dimension_id = $request->filtro['dimension_id'];
        $seccion_id = $request->filtro['seccion_id'];
        
        $where = '';

        if($medicion_id){
            $where .= " and medicions.id = $medicion_id ";
        }

        if($codSucursal){
            $where .= " and sucursals.cod_sucursal = $codSucursal ";
        }
        
        if($formato_id){
            $where .= " and formatos.id = $formato_id ";
        }

        if($dimension_id){
            $where .= " and dimensions.id = $dimension_id ";
        }

        if($seccion_id){
            $where .= " and seccions.cod_seccion = $seccion_id ";
        }

        $sql = "select 1 as cod_seccion, dimensions.id as dimension_id, sum(datos_consolidados.puntaje) as suma, count(datos_consolidados.puntaje) as cuenta, round((sum(datos_consolidados.puntaje) / count(datos_consolidados.puntaje))*100, 0) as porc ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sql .= "medicion_tarea.medicion_id = medicions.id $where ";
        $sql .= "group by cod_seccion, dimensions.id ";
        $sql .= "union ";
        $sql .= "select seccions.cod_seccion, dimensions.id as dimension_id, sum(datos_consolidados.puntaje) as suma, count(datos_consolidados.puntaje) as cuenta, round((sum(datos_consolidados.puntaje) / count(datos_consolidados.puntaje))*100, 0) as porc ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sql .= "medicion_tarea.medicion_id = medicions.id $where ";
        $sql .= "group by seccions.cod_seccion, dimensions.id ";
        $sql .= "union ";
        $sql .= "select 1 AS cod_seccion, 9 as dimension_id, sum(datos_consolidados.puntaje) as suma, count(datos_consolidados.puntaje) as cuenta, round((sum(datos_consolidados.puntaje) / count(datos_consolidados.puntaje))*100, 0) as porc ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sql .= "medicion_tarea.medicion_id = medicions.id $where ";
        $sql .= "union ";
        $sql .= "select seccions.cod_seccion, 9 as dimension_id, sum(datos_consolidados.puntaje) as suma, count(datos_consolidados.puntaje) as cuenta, round((sum(datos_consolidados.puntaje) / count(datos_consolidados.puntaje))*100, 0) as porc ";
        $sql .= "from tareas, datos_consolidados, preguntas, sucursals, formatos_sucursals, formatos, seccions, dimension_pregunta, dimensions, medicion_tarea, medicions  ";
        $sql .= "where tareas.cod_tarea = datos_consolidados.cod_tarea and datos_consolidados.cod_interno = preguntas.cod_interno and tareas.cod_sucursal = sucursals.cod_sucursal AND  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos_sucursals.formato_id = formatos.id and preguntas.cod_seccion = seccions.cod_seccion and  ";
        $sql .= "dimension_pregunta.cod_interno = preguntas.cod_interno and dimension_pregunta.dimension_id = dimensions.id and medicion_tarea.cod_tarea = tareas.cod_tarea and  ";
        $sql .= "medicion_tarea.medicion_id = medicions.id $where ";
        $sql .= "group by seccions.cod_seccion;";
        
        
        $resultadoPorFormato = [];

        $datos = DB::select($sql);
        foreach ($datos as $dato){
            $resultadoPorFormato['D'.$dato->dimension_id][] = (int) $dato->porc;
        }

        return $this->sendResponse($resultadoPorFormato, 'Listado sección y dimensiones');

    }
}
