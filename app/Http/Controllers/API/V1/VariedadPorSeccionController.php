<?php

namespace App\Http\Controllers\API\V1;

use App\Pregunta;
use App\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\PreguntaAlternativa;

class VariedadPorSeccionController extends BaseController
{
    public function getByPregunta(Request $request){
        
        $pregunta = Pregunta::where('cod_interno', $request->cod_interno)->first();
        return $this->sendResponse($pregunta, 'Puntos de cotacto');
    }

    public function getVariedadByPregunta(Request $request){

        // dd($request->all());
        $codSucursal = $request->filtro;
        $pregunta = $request->cod_interno;
        $largoPregunta = strlen($pregunta) + 3;

        // dd($codSucursal);

        $sql = "select medicions.id as medicion_id, pregunta_alternativas.cod_interno, pregunta_alternativas.literal_pregunta, datos_consolidados_alternativas.puntaje as total ";
        $sql .= "from datos_consolidados_alternativas, tareas, pregunta_alternativas, medicion_tarea, medicions ";
        $sql .= "where datos_consolidados_alternativas.cod_tarea = tareas.cod_tarea and datos_consolidados_alternativas.cod_interno = pregunta_alternativas.cod_interno and " ;
        $sql .= "tareas.cod_tarea = medicion_tarea.cod_tarea and medicion_tarea.medicion_id = medicions.id and ";
        $sql .= "tareas.cod_sucursal = $codSucursal and  pregunta_alternativas.cod_interno like '". $pregunta ."_%' and LENGTH (pregunta_alternativas.cod_interno) <= $largoPregunta order by pregunta_alternativas.literal_pregunta";

        // dd($sql);

        $variedades = DB::select($sql);

        $arrayVariedades = [];

        foreach ($variedades as $variedad){
            $arrayVariedades[$variedad->literal_pregunta]['m'.$variedad->medicion_id] = $variedad->total;
        }

        return $this->sendResponse($arrayVariedades, 'Puntos de cotacto');

    }
}
