<?php

namespace App\Http\Controllers;

use App\Medicion;
use App\Exports\BaseExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BaseDeDatosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $medicions = Medicion::orderBy('fecha_numero', 'DESC')->get();
        return view('base_de_datos.index', compact('medicions'));
    }

    public function export($medicion, $formato){

        if ($formato == 1) {
            $nombreSabana = 'base_lider.xlsx';
        }
        else if ($formato == 2) {
            $nombreSabana = 'base_express.xlsx';
        }
        else if ($formato == 3) {
            $nombreSabana = 'base_sba.xlsx';
        }

        try {
            return Excel::download(new BaseExport($medicion, $formato), $nombreSabana);
        } catch (\Throwable $th) {
            dd($th);
            return redirect('/home/dashboard');
        }
    }
}
