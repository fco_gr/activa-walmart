<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PuntoContactoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('punto_contacto.index');
    }
}
