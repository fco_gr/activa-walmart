<?php

namespace App\Http\Controllers;

use App\Dato;
use Throwable;
use App\Pregunta;
use App\Alternativa;
use App\Diccionario;
use App\DatosConsolidado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DatosConsolidadosAlternativa;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class DatosDetalleController extends Controller
{

    function SeparaMultples($dato, $codPregunta, $codInterno, $formato, $respuestas){

        // dd ($codInterno);
        // dd($dato->cod_cuestionario);
        // dd($formato);
        // dd($codInterno);
        // dd($codPregunta);
        // dd($dato);

        $formatoTXT = '';
        if ($formato == "Lider"){
            $formatoTXT = 'lider';
        }
        else if ($formato == "Lider Express"){
            $formatoTXT = 'express';
        }
        else if ($formato == "SBA"){
            $formatoTXT = 'sba';
        }

        // dd($formatoTXT);
        // dd($codPregunta);

        if ($codInterno == 'P2_1_2_6' && ($dato->cod_sucursal != 95 && $dato->cod_sucursal != 149 && $dato->cod_sucursal != 165 && $dato->cod_sucursal != 180)){
            return 1;
        }

        if ($codInterno == 'P3_2_4' && $formatoTXT == 'sba'){
            return 1;
        }

        if ($codInterno == 'P2_2_2_7' && $formatoTXT == 'sba'){
            return 1;
        }

        if ($codInterno == 'P2_2_2_7' && (
                $dato->cod_sucursal == 2114160004 || $dato->cod_sucursal == 2114160008 || $dato->cod_sucursal == 2114160060 ||
                $dato->cod_sucursal == 2114160149 || $dato->cod_sucursal == 2114160165 || $dato->cod_sucursal == 2114160180 ||
                $dato->cod_sucursal == 2114160188 || $dato->cod_sucursal == 2114160071 ||

                $dato->cod_sucursal == 2114170004 || $dato->cod_sucursal == 2114170008 || $dato->cod_sucursal == 2114170060 ||
                $dato->cod_sucursal == 2114170149 || $dato->cod_sucursal == 2114170165 || $dato->cod_sucursal == 2114170180 ||
                $dato->cod_sucursal == 2114170188 || $dato->cod_sucursal == 2114170071 ||

                $dato->cod_sucursal == 4 || $dato->cod_sucursal == 8 || $dato->cod_sucursal == 60 || $dato->cod_sucursal == 149 || 
                $dato->cod_sucursal == 165 || $dato->cod_sucursal == 180 || $dato->cod_sucursal == 188 || $dato->cod_sucursal == 71 
            )
        )
        {
            return 1;
        }

        if ($codInterno == 'P4_2_5' && (
                $dato->cod_sucursal == 2114160004 || $dato->cod_sucursal == 2114160003 || $dato->cod_sucursal == 2114160071 ||
                $dato->cod_sucursal == 2114160095 ||

                $dato->cod_sucursal == 2114170004 || $dato->cod_sucursal == 2114170003 || $dato->cod_sucursal == 2114170071 ||
                $dato->cod_sucursal == 2114170095 ||

                $dato->cod_sucursal == 4 || $dato->cod_sucursal == 3 || $dato->cod_sucursal == 71 || $dato->cod_sucursal == 95
            )
        )
        {
            return 1;
        }

        $codPreguntaNew = $dato->cod_cuestionario . "_" . $codInterno;
        //dd($codPreguntaNew);
        // $dato->cod_cuestionario
        // dd($codPreguntaNew);
        $cantidadRespuestas = Alternativa::where('cod_pregunta', $codPreguntaNew)->count();
        $alternativas = Alternativa::where('cod_pregunta', $codPreguntaNew)->orderBy('cod_pregunta')->get();

        // dd($cantidadRespuestas, $alternativas);
        $aAlternativa = [];
        
        foreach ($alternativas as $alternativa){
            $aAlternativa[$alternativa->cod_alternativa] = 0;
        }
        

        //dd($cantidadRespuestas . ' ' . $codPreguntaNew);
        $resp = explode(',', $respuestas);
        // dd(count($resp));

        // dd($cantidadRespuestas);

        $c = DatosConsolidado::create([
            'cod_estudio' => $dato->cod_estudio,
            'cod_tarea' => $dato->cod_tarea,
            'cod_pregunta' => $codPregunta,
            'cod_interno' => $codInterno,
            'respuesta' => count($resp) == $cantidadRespuestas ? 1 : 0,
            'puntaje' => count($resp) == $cantidadRespuestas ? 1 : 0,
            'puntaje_total' => count($resp) == $cantidadRespuestas ? 1 : 0,
        ]);

        if (!$respuestas){
            return 1;
        }


        // dd($aAlternativa);
        // $arrayResp = array_fill(1, $cantidadRespuestas, 0);
        $arrayResp = $aAlternativa;
        

        try {
            for ($i=0; $i<count($resp); $i++){
                $arrayResp[$resp[$i]] = 1;
            }
        } catch (Throwable $e) {
            dd($arrayResp, $resp, $e);
        }

        

        // dd($aAlternativa, $arrayResp, $resp);

        

        // for ($i=1; $i<=count($arrayResp); $i++){
        foreach($arrayResp as $key=>$val) {
            $respuesta = 0;
            $puntaje = 0;
            $puntajeTotal = 0;
            // dd("$dato->cod_tarea -- código interno $codInterno  --  $i -- $formatoTXT");
            $diccionario = Diccionario::where('pregunta_cod_pregunta', $codInterno)->where($formatoTXT, $key)->first();
            // dd($diccionario);
            $preguntaInterneNueva = $codInterno . "_" . $diccionario->pregunta_id;
            // dd($preguntaInterneNueva);

            
            try {
                
                if ($arrayResp[$key] == 1) {
                    $respuesta = 1;
                    $puntaje = 1;
                    $puntajeTotal = 1;
                }
            } catch (Throwable $e) {
                // dd($aAlternativa, $codPreguntaNew, $arrayResp, $codPregunta, $dato, $cantidadRespuestas, $e);
                dd($aAlternativa, $arrayResp, $resp, $e, $dato, $codPreguntaNew, $respuestas);
            }

            // dd($aAlternativa, $arrayResp, $resp);

            

            $c = DatosConsolidadosAlternativa::create([
                'cod_estudio' => $dato->cod_estudio,
                'cod_tarea' => $dato->cod_tarea,
                'cod_pregunta' => $codPregunta,
                'cod_interno' => $preguntaInterneNueva,
                'respuesta' => $respuesta,
                'puntaje' => $puntaje,
                'puntaje_total' => $puntajeTotal,
            ]);
        }


        // echo "$codInterno <br/>";
        // echo "$respuestas <br/>";
       
        return 1;
    }

    public function index(){
        return view('dooit/copia_detalle');
    }

    public function copiaDatosDetalle(){
        // $preguntas = Pregunta::whereIn('cod_tipo_pregunta', [4])->get()->toArray();
        $preguntas = Pregunta::whereIn('cod_tipo_pregunta', [3, 4])->get()->toArray();
        $preguntas = (array) $preguntas;
            // dd($preguntas);

        
        $sql = "select tareas.cod_estudio, tareas.cod_cuestionario, sucursals.nombre_sucursal, formatos.descripcion as formato_descripcion, sucursals.cod_sucursal, datos.* from datos, tareas, sucursals, formatos_sucursals, formatos ";
        $sql .= "where datos.cod_tarea = tareas.cod_tarea and tareas.cod_sucursal = sucursals.cod_sucursal and  ";
        $sql .= "sucursals.cod_sucursal = formatos_sucursals.cod_sucursal and formatos.id  = formatos_sucursals.formato_id and  ";
        $sql .= "tareas.cod_tarea not in (select distinct (cod_tarea) from datos_consolidados) and  ";
        $sql .= "status in (5, 6) ";
        // $sql .= "status in (5, 6) and tareas.cod_tarea = 2114160023";

        // dd($sql);

        $datos = DB::select($sql);

        foreach ($datos as $dato){
            // dd($dato);
            
            $codEstudio = $dato->cod_estudio;
            $codTarea = $dato->cod_tarea;
            $formato = $dato->formato_descripcion;
            $codSucursal = $dato->cod_sucursal;
            for ($i=0; $i<count($preguntas); $i++){
                $cargaDato = false;
                $puntaje = 0;
                $puntajeTotal = 0;
                $tipoPregunta = $preguntas[$i]['cod_tipo_pregunta'];
                $codInterno = $preguntas[$i]['cod_interno'];
                $codPregunta = $preguntas[$i]['cod_pregunta'];
                
                if ( $tipoPregunta== 3) {
                    $respuesta = $dato->$codInterno; 
                    // dd($codInterno, $respuesta);

                    if ($respuesta){
                        $inserta = true;
                        if ($respuesta == 1){
                            $puntaje = 1;
                            $puntajeTotal = 1; 
                        }
                        else if  ($respuesta == 2){
                            $puntaje = 0;
                            $puntajeTotal = 0;
                        }
                        else {
                            $inserta = false;
                        }

                        if ($codInterno == 'P2_2_2_5' && 
                            (
                                $codSucursal == 2114160149 || $codSucursal == 2114160188 || $codSucursal == 2114160071 ||
                                $codSucursal == 2114170149 || $codSucursal == 2114170188 || $codSucursal == 2114170071 ||
                                $codSucursal == 149 || $codSucursal == 188 || $codSucursal == 71
                            )
                        ) {
                            $inserta = false;
                        }

                        if ($codInterno == 'P4_2_2' && 
                            (
                                $codSucursal == 2114160004 || $codSucursal == 2114160003 || $codSucursal == 2114160071 || $codSucursal == 2114160095 || 
                                $codSucursal == 2114170004 || $codSucursal == 2114170003 || $codSucursal == 2114170071 || $codSucursal == 2114170095 ||
                                $codSucursal == 4 || $codSucursal == 3 || $codSucursal == 71 || $codSucursal == 95
                            )
                        ) {
                            $inserta = false;
                        }



                        if ($inserta){
                            $c = DatosConsolidado::create([
                                'cod_estudio' => $codEstudio,
                                'cod_tarea' => $codTarea,
                                'cod_pregunta' => $codPregunta,
                                'cod_interno' => $codInterno,
                                'respuesta' => $respuesta,
                                'puntaje' => $puntaje,
                                'puntaje_total' => $puntajeTotal,
                            ]);
                        }
                    }
                }
                else {

                    $respuestas = $dato->$codInterno;
                    // dd($respuestas. ' - ' . $formato . ' - ' . $codInterno);
                    $puntajeb = $this->SeparaMultples($dato, $codPregunta, $codInterno, $dato->formato_descripcion, $respuestas);
                    
                }
            }
        }

        // dd('fin');
        
        return back()->withInput();
    }
}
