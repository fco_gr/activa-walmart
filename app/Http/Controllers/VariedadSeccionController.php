<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VariedadSeccionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('variedad_por_seccion.index');
    }
}
