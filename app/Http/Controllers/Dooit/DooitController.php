<?php

namespace App\Http\Controllers\Dooit;

use App\Alternativa;
use App\Dato;
use App\Tarea;
use App\Estudio;
use App\Seccion;
use App\Pregunta;
use App\Sucursal;
use App\Cuestionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DooitController extends Controller
{
    public function getCuestionarios(){

        $estudios = Estudio::where("sincronizar", true)->get();

        foreach($estudios as $estudio){
            $codEstudio = $estudio->cod_estudio;
            // dd($codEstudio);

            $cuestionariosCargados = Cuestionario::where('cod_estudio', $codEstudio)->pluck('cod_cuestionario')->toArray();
            //dd($cuestionarios);

            $cuestionarioDooits = DB::connection('mysql_connect_2')->table('cuestionarios')
                ->where('cod_estudio', $codEstudio)
                ->whereNotIn('cod_cuestionario', $cuestionariosCargados)
                ->get()->toArray();

            //  dd($cuestionarioDooits);
            foreach ($cuestionarioDooits as $cuestionarioDooit){
                $cuestionarioDooitArray = (array) $cuestionarioDooit;
                // $cuestionarioDooitArray['id'] = null;
                $c = Cuestionario::insert($cuestionarioDooitArray);
            }
        }
        return true;
    }

    public function getSecciones(){

        $estudios = Estudio::where("sincronizar", true)->get();

        foreach($estudios as $estudio){
            $codEstudio = $estudio->cod_estudio;
            $codCuestionario = $estudio->cod_cuestionario;
            // dd($codEstudio);

            $seccionCargados = Seccion::where('cod_estudio', $codEstudio)
                ->where('cod_cuestionario', $codCuestionario)
                ->pluck('cod_seccion')->toArray();
            
                // dd($seccionCargados);

            $seccionesDooits = DB::connection('mysql_connect_2')->table('secciones')
                ->where('cod_estudio', $codEstudio)
                ->where('cod_cuestionario', $codCuestionario)
                ->whereNotIn('cod_cod_seccion', $seccionCargados)
                ->get()->toArray();

            //dd($seccionesDooits);

            foreach ($seccionesDooits as $cuestionarioDooit){
                $seccionDooitArray = (array) $cuestionarioDooit;
                // $seccionDooitArray['id'] = null;
                $c = Seccion::insert($seccionDooitArray);
                // dd($c);
            }

        }

        return "Exito";
        
    }

    public function getPreguntas(){

        $estudios = Estudio::where("sincronizar", true)->get();

        foreach($estudios as $estudio){
            $codEstudio = $estudio->cod_estudio;
            $codCuestionario = $estudio->cod_cuestionario;
            // dd($codEstudio);

            $preguntasCargados = Pregunta::where('cod_estudio', $codEstudio)
                ->where('cod_cuestionario', $codCuestionario)
                ->pluck('cod_pregunta')->toArray();
            
                // dd($preguntasCargados);

            $preguntasDooits = DB::connection('mysql_connect_2')->table('preguntas')
                ->where('cod_estudio', $codEstudio)
                ->where('cod_cuestionario', $codCuestionario)
                ->whereNotIn('cod_sucursal', $preguntasCargados)
                ->get()->toArray();

            // dd($preguntasDooits);
            foreach ($preguntasDooits as $cuestionarioDooit){
                $preguntaDooitArray = (array) $cuestionarioDooit;
                // $preguntaDooitArray['id'] = null;
                $c = Pregunta::insert($preguntaDooitArray);
                // dd($c);
            }

        }

        return true;
        
    }

    public function getAlternativas(){

        $estudios = Estudio::where("sincronizar", true)->get();

        foreach($estudios as $estudio){
            $codEstudio = $estudio->cod_estudio;
            // $codCuestionario = $estudio->cod_cuestionario;
            //dd($codEstudio . ' - - '  . $codEstudio);

            $cuestionarios = Cuestionario::where('cod_estudio', $codEstudio)->get();

            foreach($cuestionarios as $cuestionario){
                $codCuestionario = $cuestionario['cod_cuestionario'];
                // dd($codCuestionario);

                $alternativasDooits = DB::connection('mysql_connect_2')->table('alternativas')
                    ->where('cod_estudio', $codEstudio)
                    ->where('cod_cuestionario', $codCuestionario)
                    ->get()->toArray();
                
                //dd($alternativasDooits);

                foreach ($alternativasDooits as $alternativa){
                    //dd($alternativa);
                    Alternativa::updateOrCreate(
                        [
                            'cod_estudio' => $alternativa->cod_estudio,
                            'cod_cuestionario' => $alternativa->cod_cuestionario,
                            'cod_pregunta' => $alternativa->cod_pregunta,
                            'cod_alternativa' => $alternativa->cod_alternativa,
                            'literal_alternativa' => $alternativa->literal_alternativa,
                            'orden' => $alternativa->orden,
                            'estado' => $alternativa->estado,
                        ],
                        [
                            'id_alternativa' => $alternativa->id_alternativa
                        ]
                    );
                }

                

            }

            
            

            

            // dd($preguntasDooits);
            // foreach ($preguntasDooits as $cuestionarioDooit){
            //     $preguntaDooitArray = (array) $cuestionarioDooit;
            //     // $preguntaDooitArray['id'] = null;
            //     $c = Pregunta::insert($preguntaDooitArray);
            //     // dd($c);
            // }

        }

        return true;
        
    }
 

    public function getSucursales(){

        $estudios = Estudio::where("sincronizar", true)->get();

        foreach($estudios as $estudio){
            $codEstudio = $estudio->cod_estudio;
            // dd($codEstudio);

            $sucursalesCargados = Sucursal::where('cod_estudio', $codEstudio)->pluck('cod_sucursal')->toArray();
            // dd($sucursalesCargados);

            $sucursalDooits = DB::connection('mysql_connect_2')->table('sucursales')
                ->where('cod_estudio', $codEstudio)
                ->whereNotIn('cod_sucursal', $sucursalesCargados)
                ->get()->toArray();

            // dd($sucursalDooits);
            foreach ($sucursalDooits as $cuestionarioDooit){
                $cuestionarioDooitArray = (array) $cuestionarioDooit;
                // $cuestionarioDooitArray['id'] = null;
                $c = Sucursal::insert($cuestionarioDooitArray);
                // dd($c);
            }

        }

        return true;
        
    }

    public function getTareas(){

        $estudios = Estudio::where("sincronizar", true)->get();

        foreach($estudios as $estudio){
            $codEstudio = $estudio->cod_estudio;
            // dd($codEstudio);

            $tareasCargados = Tarea::where('cod_estudio', $codEstudio)
                ->pluck('cod_tarea')->toArray();
            //dd($tareasCargados);

            $tarealDooits = DB::connection('mysql_connect_2')->table('tareas')
                ->where('cod_estudio', $codEstudio)
                ->whereIn('status', [5, 6])
                ->whereNotIn('cod_tarea', $tareasCargados)
                ->get()->toArray();

            // dd($tarealDooits);
            foreach ($tarealDooits as $tareaDooit){
                $tareaDooitArray = (array) $tareaDooit;
                $tareaDooitArray['cod_sucursal'] = $tareaDooitArray['cod_sucursal'] % 10000;
                $tarea = Tarea::insert($tareaDooitArray);

                // dd('por acá');

                $tablaDato = 'datos_' . $tareaDooitArray['cod_cuestionario'];
                $codTarea = $tareaDooitArray['cod_tarea'];
                
                $dato = DB::connection('mysql_connect_2')->table($tablaDato)
                    ->where('cod_tarea', $codTarea)
                    ->first();
                
                $datoArray = (array) $dato;

                $localP = array_key_exists('LOCAL3', $datoArray) ? $datoArray['LOCAL3'] : 0;
                
                unset($datoArray['LOCAL']);
                $datoArray['LOCAL'] = '';
                unset($datoArray['LOCAL2']);
                unset($datoArray['LOCAL3']);

                //TODO select cod_sucursal, tareas.* from tareas;

                if ($localP == 6){
                    $datoArray['P3_2_4'] = $datoArray['P3_2_4_1'];
                }
                unset($datoArray['P3_2_4_1']);

                $dato = Dato::insert($datoArray);

            }

        }

        return true;
        
    }
}
