<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $fillable = [
        'id', 'cod_tarea', 'nombre', 'nivel', 'pago', 'cod_estudio', 'descripcion', 'cod_dooer', 
        'cod_sucursal', 'fecha_inicio', 'horas', 'status', 'fecha_asignacion', 'fecha_completacion', 
        'fecha_validacion', 'latitud_inicial', 'longitud_inicial', 'latitud_final', 'longitud_final', 
        'ubicacion_inicial', 'ubicacion_final', 'created_at', 'updated_at'
    ];
}
