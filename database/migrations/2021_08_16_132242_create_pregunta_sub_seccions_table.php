<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaSubSeccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_sub_seccions', function (Blueprint $table) {
            $table->string('cod_interno', 50)->primary();
            $table->unsignedBigInteger('sub_seccion_id');
            $table->string('literal_sub_seccion', 50);
            $table->timestamps();

            $table->foreign('cod_interno')
                ->references('cod_interno')
                ->on('preguntas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_sub_seccions');
    }
}
