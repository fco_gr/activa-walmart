<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormatosSucursalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formatos_sucursals', function (Blueprint $table) {
            $table->integer('cod_sucursal')->unique();
            $table->unsignedBigInteger ('formato_id');

            $table->foreign('formato_id')
                ->references('id')
                ->on('formatos');

            $table->foreign('cod_sucursal')
                ->references('cod_sucursal')
                ->on('sucursals');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formatos_sucursals');
    }
}
