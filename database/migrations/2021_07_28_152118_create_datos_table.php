<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos', function (Blueprint $table) {
            // $table->id();
            $table->integer('cod_tarea')->primary();
            $table->string('cod_dooer');
            $table->datetime('fecha')->nullable();
            $table->date('FEC_EVAL')->nullable();
            $table->time('HR_ENTRADA')->nullable();
            $table->time('HR_SALIDA')->nullable();
            $table->string('LOCAL');

            $table->integer('P1_1_1')->nullable();
            $table->text('P1_1_1_TXT')->nullable();
            $table->integer('P1_1_2')->nullable();
            $table->text('P1_1_2_TXT')->nullable();
            $table->integer('P1_1_3')->nullable();
            $table->integer('P1_1_4')->nullable();
            $table->text('P1_1_4_TXT')->nullable();
            $table->integer('P1_1_5')->nullable();
            $table->text('P1_1_5_TXT')->nullable();
            $table->integer('P1_1_6')->nullable();
            $table->text('P1_1_6_TXT')->nullable();

            $table->integer('P1_2_1')->nullable();
            $table->text('P1_2_1_TXT')->nullable();
            $table->integer('P1_2_2')->nullable();
            $table->text('P1_2_2_TXT')->nullable();
            $table->integer('P1_2_3')->nullable();
            $table->text('P1_2_3_TXT')->nullable();
            $table->string('P1_2_4', 50)->nullable();
            $table->string('P1_2_5', 50)->nullable();

            $table->integer('P1_3_1')->nullable();
            $table->text('P1_3_1_TXT')->nullable();
            $table->integer('P1_3_2')->nullable();
            $table->text('P1_3_2_TXT')->nullable();
            $table->text('FOTO_FFVV')->nullable();
            
            $table->integer('P0_2')->nullable();
            $table->integer('P2_1_1_1')->nullable();
            $table->text('P2_1_1_1_T')->nullable();
            $table->integer('P2_1_1_2')->nullable();
            $table->text('P2_1_1_2_T')->nullable();

            $table->integer('P2_1_2_1')->nullable();
            $table->text('P2_1_2_1_T')->nullable();
            $table->integer('P2_1_2_2')->nullable();
            $table->text('P2_1_2_2_T')->nullable();
            $table->integer('P2_1_2_3')->nullable();
            $table->text('P2_1_2_3_T')->nullable();
            $table->string('P2_1_2_6')->nullable();
            $table->integer('P2_1_2_4')->nullable();
            $table->text('P2_1_2_4_T')->nullable();

            $table->integer('P2_1_3_1')->nullable();
            $table->text('P2_1_3_1_T')->nullable();
            $table->text('P2_1_3_2')->nullable();
            $table->integer('P2_1_3_3')->nullable();
            $table->text('P2_1_3_3_T')->nullable();
            $table->integer('P2_1_3_4')->nullable();
            $table->text('P2_1_3_4_T')->nullable();

            $table->integer('P2_2_1_1')->nullable();
            $table->text('P2_2_1_1_T')->nullable();
            $table->integer('P2_2_1_2')->nullable();
            $table->text('P2_2_1_2_T')->nullable();

            $table->integer('P2_2_2_1')->nullable();
            $table->text('P2_2_2_1_T')->nullable();
            $table->integer('P2_2_2_2')->nullable();
            $table->text('P2_2_2_2_T')->nullable();
            $table->integer('P2_2_2_3')->nullable();
            $table->text('P2_2_2_3_T')->nullable();
            $table->string('P2_2_2_6')->nullable();
            $table->string('P2_2_2_7')->nullable();
            $table->integer('P2_2_2_4')->nullable();
            $table->text('P2_2_2_4_T')->nullable();
            $table->integer('P2_2_2_5')->nullable();
            $table->text('P2_2_2_5_T')->nullable();
            $table->integer('P2_1_2_5')->nullable();
            $table->text('P2_1_2_5_T')->nullable();
            $table->text('FOTO_CARNE')->nullable();

            $table->integer('P3_1_1')->nullable();
            $table->text('P3_1_1_TXT')->nullable();
            $table->integer('P3_1_3')->nullable();
            $table->text('P3_1_3_TXT')->nullable();
            $table->integer('P3_1_2')->nullable();
            $table->text('P3_1_2_TXT')->nullable();

            $table->integer('P3_2_1')->nullable();
            $table->text('P3_2_1_TXT')->nullable();
            $table->integer('P3_2_2')->nullable();
            $table->text('P3_2_2_TXT')->nullable();
            $table->integer('P3_2_3')->nullable();
            $table->text('P3_2_3_TXT')->nullable();
            $table->string('P3_2_4', 50)->nullable();
            $table->integer('P3_2_5')->nullable();
            $table->text('P3_2_5_TXT')->nullable();
            $table->string('P3_2_5_1', 50)->nullable();

            $table->integer('P3_3_1')->nullable();
            $table->text('P3_3_1_TXT')->nullable();
            $table->text('P3_3_2')->nullable();
            $table->integer('P3_3_3')->nullable();
            $table->text('P3_3_3_TXT')->nullable();
            $table->integer('P3_3_4')->nullable();
            $table->text('P3_3_4_TXT')->nullable();
            $table->integer('P3_3_5')->nullable();
            $table->text('P3_3_5_TXT')->nullable();

            $table->integer('P3_1_1_1')->nullable();
            $table->text('P3_1_1_1_TXT')->nullable();
            $table->integer('P3_1_2_2')->nullable();
            $table->text('P3_1_2_2_TXT')->nullable();
            $table->integer('P3_2_1_3')->nullable();
            $table->text('P3_2_1_3_TXT')->nullable();
            $table->integer('P3_2_2_4')->nullable();
            $table->text('P3_2_2_4_TXT')->nullable();
            $table->integer('P3_2_3_5')->nullable();
            $table->text('P3_2_3_5_TXT')->nullable();
            $table->string('P3_2_4_6', 50)->nullable();
            $table->text('FOTO_FIAMB')->nullable();

            $table->integer('P4_1_1')->nullable();
            $table->text('P4_1_1_TXT')->nullable();
            $table->text('P4_1_2')->nullable();
            $table->integer('P4_1_3')->nullable();
            $table->text('P4_1_3_TXT')->nullable();
            $table->integer('P4_1_4')->nullable();
            $table->text('P4_1_4_TXT')->nullable();
            $table->integer('P4_1_5')->nullable();
            $table->text('P4_1_5_TXT')->nullable();

            $table->integer('P4_2_1')->nullable();
            $table->text('P4_2_1_TXT')->nullable();
            $table->integer('P4_2_2')->nullable();
            $table->text('P4_2_2_TXT')->nullable();
            $table->integer('P4_2_3')->nullable();
            $table->text('P4_2_3_TXT')->nullable();
            $table->string('P4_2_4', 50)->nullable();
            $table->string('P4_2_5', 50)->nullable();

            $table->integer('P4_3_1')->nullable();
            $table->text('P4_3_1_TXT')->nullable();
            $table->text('P4_3_2')->nullable();
            $table->integer('P4_3_3')->nullable();
            $table->text('P4_3_3_TXT')->nullable();
            $table->integer('P4_3_4')->nullable();
            $table->text('P4_3_4_TXT')->nullable();
            $table->text('FOTO_PAN')->nullable();
            $table->text('OBS_GENE')->nullable();
            
            $table->timestamps();

            $table->foreign('cod_tarea')
                ->references('cod_tarea')
                ->on('tareas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos');
    }
}
