<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaAlternativasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_alternativas', function (Blueprint $table) {
            $table->string('cod_pregunta')->primary();
            $table->string('cod_interno', 50)->unique();
            $table->string('cod_estudio', 10);
            $table->integer('cod_cuestionario');
            $table->Integer('cod_seccion');
            $table->string('literal_pregunta', 600);
            $table->integer('cod_tipo_pregunta');
            $table->text('condicion')->nullable();
            $table->integer('estado')->default(1);
            $table->integer('orden')->default(0);
            $table->integer('teclado')->nullable();
            $table->timestamps();

            $table->foreign('cod_estudio')
                ->references('cod_estudio')
                ->on('estudios');

            $table->foreign('cod_cuestionario')
                ->references('cod_cuestionario')
                ->on('cuestionarios');

            $table->foreign('cod_seccion')
                ->references('cod_seccion')
                ->on('seccions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_alternativas');
    }
}
