<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccions', function (Blueprint $table) {
            // $table->id();
            $table->integer('cod_seccion')->primary();
            $table->string('cod_estudio');
            $table->integer('cod_cuestionario');
            $table->string('titulo_seccion');
            $table->integer('estado')->nullable()->default(null);
            $table->integer('orden')->nullable()->default(null);
            $table->string('condicion', 100)->nullable()->default(null);
            $table->timestamps();

            $table->foreign('cod_estudio')
                ->references('cod_estudio')
                ->on('estudios');

            $table->foreign('cod_cuestionario')
                ->references('cod_cuestionario')
                ->on('cuestionarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seccions');
    }
}
