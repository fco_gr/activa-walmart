<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicions', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->integer('quincena');
            $table->string('descripcion_corto')->nullable();
            $table->unsignedBigInteger('year');
            $table->unsignedBigInteger('month_id');
            $table->integer('fecha_numero');
            $table->string('observacion')->nullable();
            $table->boolean('activo')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicions');
    }
}
