<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursals', function (Blueprint $table) {
            // $table->id();
            $table->integer('cod_sucursal')->primary();
            $table->string('cod_estudio');
            $table->string('nombre_sucursal');
            $table->integer('cod_pais_nivel')->nullable();
            $table->string('direccion');
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            $table->point('ubicacion')->nullable();
            $table->text('adicionales')->nullable();
            $table->boolean('estado')->nullable();
            $table->timestamps();


            $table->foreign('cod_estudio')
                ->references('cod_estudio')
                ->on('estudios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursals');
    }
}
