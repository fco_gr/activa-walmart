<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosConsolidadosAlternativasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_consolidados_alternativas', function (Blueprint $table) {
            $table->id();
            $table->string('cod_estudio', 10);
            $table->integer('cod_tarea');
            $table->string('cod_pregunta', 30);
            $table->string('cod_interno', 30);
            $table->integer('respuesta');
            $table->integer('puntaje');
            $table->integer('puntaje_total');
            $table->timestamps();

            $table->foreign('cod_interno')
                ->references('cod_interno')
                ->on('pregunta_alternativas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_consolidados_alternativas');
    }
}
