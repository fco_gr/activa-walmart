<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicionTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicion_tarea', function (Blueprint $table) {
            $table->unsignedBigInteger('medicion_id');
            $table->integer('cod_tarea');

            $table->foreign('medicion_id')
                ->references('id')
                ->on('medicions');

            $table->foreign('cod_tarea')
                ->references('cod_tarea')
                ->on('tareas');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicion_tarea');
    }
}
