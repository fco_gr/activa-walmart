<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlternativasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternativas', function (Blueprint $table) {
            $table->string('id_alternativa')->primary();
            $table->string('cod_estudio', 10);
            $table->integer('cod_cuestionario');
            $table->string('cod_pregunta');
            $table->string('cod_alternativa', 50);
            $table->text('literal_alternativa');
            $table->integer('orden');
            $table->integer('estado');
            $table->timestamps();

            $table->foreign('cod_estudio')
                ->references('cod_estudio')
                ->on('estudios');

            $table->foreign('cod_cuestionario')
                ->references('cod_cuestionario')
                ->on('cuestionarios');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alternativas');
    }
}
