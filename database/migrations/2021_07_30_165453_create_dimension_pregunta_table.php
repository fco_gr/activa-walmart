<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimensionPreguntaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimension_pregunta', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dimension_id');
            $table->string('cod_interno', 50);
            $table->timestamps();

            $table->foreign('dimension_id')
                ->references('id')
                ->on('dimensions');

            $table->foreign('cod_interno')
                ->references('cod_interno')
                ->on('preguntas');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimension_pregunta');
    }
}
