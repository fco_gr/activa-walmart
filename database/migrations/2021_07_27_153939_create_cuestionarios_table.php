<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios', function (Blueprint $table) {
            // $table->id();
            $table->integer('cod_cuestionario')->primary();
            $table->string('cod_estudio');
            $table->string('titulo_cuestionario');
            $table->text('descripcion')->nullable();
            $table->boolean('estado')->defaul(false);
            $table->datetime('fecha_creacion');
            $table->string('autor')->nulable();
            $table->boolean('pasos_completos')->defaul(false);
            $table->timestamps();

            $table->foreign('cod_estudio')
                ->references('cod_estudio')
                ->on('estudios');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios');
    }
}
