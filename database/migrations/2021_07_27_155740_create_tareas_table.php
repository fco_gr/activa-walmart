<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTareasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tareas', function (Blueprint $table) {
            // $table->id();
            $table->integer('cod_tarea')->primary();
            $table->string('nombre', 50);
            $table->integer('nivel')->nullable();
            $table->string('pago', 50);
            $table->string('cod_estudio');
            $table->integer('cod_cuestionario');
            $table->text('descripcion');
            $table->integer('cod_dooer')->nullable();
            $table->integer('cod_sucursal');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_termino')->nullable();
            $table->integer('horas')->nullable();
            $table->integer('mins')->nullable();
            $table->integer('status')->nullable();
            $table->date('fecha_asignacion')->nullable();
            $table->date('fecha_completacion')->nullable();
            $table->date('fecha_validacion')->nullable();
            $table->text('latitud_inicial')->nullable();
            $table->text('longitud_inicial')->nullable();
            $table->text('latitud_final')->nullable();
            $table->text('longitud_final')->nullable();

            $table->point('ubicacion_inicial')->nullable();
            $table->point('ubicacion_final')->nullable();

            $table->timestamps();

            $table->foreign('cod_estudio')
                ->references('cod_estudio')
                ->on('estudios');

            $table->foreign('cod_cuestionario')
                ->references('cod_cuestionario')
                ->on('cuestionarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tareas');
    }
}
